/*
* alarm.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "alarm.h"
#include "chrono.h"

alarm_t::alarm_t (double tt) {
  triggert = tt;
  startt = nowt = 0;
	elapsed = 0;
  active = 0;
}

int alarm_t::operator() (double t) {
	nowt = t;
  elapsed = nowt - startt;
	if (elapsed < triggert) 
		return 0;
	else {
		startt = nowt  - elapsed + triggert;
		return 1;
	}
}

double alarm_t::operator() () {
	return (elapsed * 1.0 / triggert);
}

void alarm_t::start (double delta) {
	elapsed = 0.0;
  active = 1;
  startt = ui_clk () - delta;
}

void alarm_t::stop () {
  active = 0;
}
