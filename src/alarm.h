/*
* alarm.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __ALARM__
#define __ALARM__

struct alarm_t { 
	double triggert;
  double startt;
	double nowt;
  double elapsed;
  int active;
  alarm_t (double tt = 1.0); 
  int operator() (double t);
	double operator() ();
  void start (double delta = 0.0);
  void stop ();
};

typedef alarm_t alarmt;

#endif
