/*
* angle.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __anglet__
#define __anglet__

#include <fstream>

struct anglet {

  float deg;
  float rad;

  anglet (float d) {
    operator= (d);
  }

  anglet () {
    deg = rad = 0.0f;
  }

  anglet& operator= (const float& v) {
    deg = v;
    torad ();
    return *this;
  }

  anglet& operator+= (const float& v) {
    deg += v;
    torad ();
    return *this;
  }

  void torad () {
    extern const float PI_BY_180;
    rad = deg * PI_BY_180;
  }

};

std::ostream& operator<< (std::ostream& f, anglet& a);
std::istream& operator>> (std::istream& f, anglet& a);

#endif
