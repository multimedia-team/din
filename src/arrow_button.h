/*
* arrow_button.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __arrow_button
#define __arrow_button

#include "button.h"

struct arrow_button : button {

	enum {left = 1, right, up, down};
  int dir;

	int v[6];

  arrow_button (int sze = 12, int dirr = right);

  void draw ();

  void set_dir (int _dir);

	void set_pos (int x, int y) {
		widget::set_pos (x, y);
		set_size (size);
		set_dir (dir);
	}

	void set (int _sz, int _dir, float r = 1.0f, float g = 1.0f, float b = 1.0f) {
		set_size (_sz);
		set_dir (_dir);
		set_color (r, g, b);
	}

	void toggle ();
	void update () {}

};
#endif
