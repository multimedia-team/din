/*
* authors_note.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __authors_note
#define __authors_note

#include "ui.h"
#include "button.h"
#include <vector>

struct authors_note : ui, click_listener {

  button buy_now;
  button buy_later;

  int rollup;

	authors_note ();
  void enter ();
  void leave ();
	int handle_input ();
	void draw ();
  void setup ();
	void clicked (button& b);

};

extern authors_note anote;

#endif
