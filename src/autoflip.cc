/*
* autoflip.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "defvelaccel.h"
#include "autoflip.h"
#include "console.h"


int autoflipt::calc (float& theta, defvelaccel& d) {
  float nexttotal = total + theta;
  int flipdir = 0;
  if (nexttotal >= angle.rad) {
    flipdir = 1;
    theta = angle.rad - total;
    total = 0.0f;
    if (rndang) angle = d.autos.flip.deg (1);
  } else 
    total = nexttotal;
  return flipdir;
}

autoflipt::autoflipt (defvelaccel& d) {
  yes = d.autos.flip.yes;
  angle = d.autos.flip.deg();
  rndang = d.autos.flip.deg.rndrd;
  total = 0.0f;
}

std::istream& operator>> (std::istream& file, autoflipt& af) {
  float d;
  file >> af.yes >> af.rndang >> d >> af.total;
  af.angle = d;
  return file;
}


std::ostream& operator<< (std::ostream& file, autoflipt& af) {
  extern const char spc;
  file << af.yes << spc << af.rndang << spc << af.angle.deg << spc << af.total;
  return file;
}
