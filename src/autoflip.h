/*
* autoflip.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __autoflip
#define __autoflip

#include "angle.h"

struct defvelaccel;

struct autoflipt {
  int yes;
  anglet angle;
  int rndang;
  float total;
  autoflipt (defvelaccel& d);
  int calc (float& theta, defvelaccel& d);
  inline void set (float deg) {angle = deg;}
};

std::istream& operator>> (std::istream& file, autoflipt& af);
std::ostream& operator<< (std::ostream& file, autoflipt& af);

#endif
