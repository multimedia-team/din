/*
* autopause.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __autopause
#define __autopause

#include "alarm.h"
#include <fstream>

struct defvelaccel;
struct autorotator;
struct valt;

struct autopauset {
  int yes;
  int tar;
  int a;
  double every, f0r;
  int rndt [2];
  alarmt alm[2];
  double* t[2];
  valt* vt[2];
  autopauset (defvelaccel& d);
  void settrigt (int i, double& d);
  void setyes (int y, autorotator& ar);
  void go ();
  void operator() (autorotator& ar);
};

std::istream& operator>> (std::istream& file, autopauset& ap);
std::ostream& operator<< (std::ostream& file, autopauset& ap);

#endif
