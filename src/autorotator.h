/*
* autorotator.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __autorotator
#define __autorotator

#include "alarm.h"
#include "autoflip.h"
#include "autopause.h"

struct defvelaccel;


struct autorotator {

  defvelaccel& dva;

  int yes;
  int yess ();
  void setyes (int y);

  enum {CLOCKWISE, ANTICLOCKWISE, RANDOM}; 
  int dir; // +1 = anti-clockwise , -1 = clockwise
  int flipdir;
  autoflipt autoflip;

  autopauset autopause;

  enum {SMOOTH, TICK};
  int mov;

  alarm_t tik;
  float deg;
  float tps;

  float rpm;

  struct anglett {
    float per;
    float theta;
    anglett () {
      per = 0.0f;
      theta = 0.0f;
    }
  } angle;

  autorotator (defvelaccel& d); 

  void set_rpm (float r);

  void calc (double dt);

  inline void setdeg (float d) {
    deg = d;
    if (deg < 0.0f) deg = 0.0f;
    extern const float PI_BY_180;
    angle.per = deg * PI_BY_180;
  }

  inline void settps (float t) {
    tps = t;
    if (tps < 1.0) tps = 1.0;
    tik.triggert = 1.0 / tps;
  }

  void chgdeg (float dd);
  void chgtps (float t);

}; 

std::ostream& operator<< (std::ostream&, autorotator& ar);
std::istream& operator>> (std::istream&, autorotator& ar);

#endif
