/*
* ball_ops.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __ballops__
#define __ballops__

#include "alarm.h"
#include "random.h"

struct ball;

struct ball_op {
	static const int NUM_OPS = 5; // change when adding new op!
	static const char* names [];
	static float TRIGGERT;
	alarm_t alarm; 
	virtual int eval (ball* b = 0);
	virtual void start (ball* b) {alarm.start ();}
};

struct turn : ball_op {

	static float CLOCKWISE, ANTI_CLOCKWISE;
	rnd<float> rd;

	float vx, vy;
	float angle;
	void gen_angle (ball* b);

	turn ();
	int eval (ball* b);
	void start (ball* b);

};

struct speed : ball_op {

	static float BRAKE, ACCELERATE;
	rnd<float> rd;

	float start, delta;
	void gen_delta (ball* b);

	float max;

	speed ();
	int eval (ball* b);

};

struct teleport : ball_op {
	static float MAX_RADIUS;
	rnd<float> rd;
	float radius;
	teleport ();
	int eval (ball* b);
};

struct Clone : ball_op {
  int n;
  int max;
  float offset;
  int clone_can_clone;
  static int max_balls;
  Clone ();
  int eval (ball* b);
};

struct Transform : ball_op {
	static int rules [3];
	Transform ();
	int eval (ball* b);
};

#endif
