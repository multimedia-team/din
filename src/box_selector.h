/*
* box_selector.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#ifndef __box_selector
#define __box_selector

#include "box.h"
#include "dingl.h"
#include "widget.h"

extern int mousex, mouseyy;
extern int is_menu_visible ();

struct region_listener {
	virtual void region_begin () = 0;
	virtual const box<float>& region_update () = 0;
	virtual void region_end () = 0;
	virtual void region_abort () {}
};

struct box_selector : widget {

	// selection
	enum {NONE = 0, EXISTS, FINISH};
	int op;
	int lmb_clicked, rmb_clicked;
	region_listener* lis;

	// gl
	float boxp [12];
	int cross;

	box_selector ();
  
	void set_listener (region_listener* _lis) {
		lis = _lis;
	}

	int handle_input ();
	int abort ();
  
	inline int operator() () {return (op == EXISTS);}

	virtual void draw (const box<float>& region);
	void draw (const box<float>& region, int cross);

};

extern box_selector mon_selector;

#endif
