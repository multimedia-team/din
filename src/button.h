/*
* button.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#ifndef __button
#define __button

#include "widget.h"
#include "label.h"

#include <string>

struct button;

struct click_listener : voiddata {
  virtual void clicked (button& b) = 0;
};

struct button : label {

  int click;

  int click_repeat;
  double start_time, repeat_time;
	double first_repeat_time, subsequent_repeat_time;

  button ();

  int handle_input ();
	click_listener *lsnr;

	void set_listener (click_listener* l) {lsnr = l;}
	int call_listener ();

  void draw ();

};

#define DECL_CLICK_LISTENER(name) \
struct name : click_listener { \
	void clicked (button& b);\
};\

#define MAKE_CLICK_LISTENER(name,var) \
struct name : click_listener {\
	void clicked (button& b);\
};\
name var;\

#define CLICKED_BUTTON(scope,name) void scope::name::clicked (button& b)

#endif
