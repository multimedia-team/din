/*
* capturer.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __capturer
#define __capturer

#include "label.h"
#include "arrow_button.h"
#include "filled_button.h"
#include "state_button.h"
#include "checkbutton.h"
#include "cross_button.h"
#include "utils.h"

#include <map>

struct mouse_macro;

struct curve_editor;
struct capturer_t : widget, click_listener, move_listener {

	curve_editor* ed;

	label title;

	int folded;
	arrow_button fold;

	arrow_button play;
	cross_button kill;
	button all, none, invert;

	int sbx, sby;
	std::vector<state_button*> caps;
	int ncaps;

	sel _sel;
	desel _desel;
	togg _togg;
	void on_caps (const item_op& o);

	std::map<state_button*, point<int> > sbpt;

	void set (mouse_macro& mm, int x, int y);

  void setup (curve_editor* ce);
  int handle_input ();
	void moved ();
  void draw ();

	state_button* add ();
	void remove ();
	void rearrange ();

	void clicked (button& b);

	static int ref;
	capturer_t ();
  ~capturer_t ();

  void evalchk ();

};

#endif
