/*
* checkbutton.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#include "checkbutton.h"
using namespace std;

checkbutton::checkbutton () : state (0), lsnr (0), colorize_ (1) {
  set_state (0);
  button::set_listener (this);
}

void checkbutton::clicked (button& b) {
  toggle ();
}


void checkbutton::set_state (int s, int cl) {
  if (s) turn_on (cl); else turn_off (cl);
}

void checkbutton::turn_on (int call) {
  if (colorize_) set_color (on_color);
  if (state != 1) {
    state = 1;
    if (call && lsnr) lsnr->changed (*this);
  }
}

void checkbutton::turn_off (int call) {
  if (colorize_) set_color (off_color);
  if (state != 0) {
    state = 0;
		if (call && lsnr) lsnr->changed (*this);
  }
}

void checkbutton::toggle () {
  if (state) turn_off (); else turn_on ();
}

void checkbutton::blend_on_off_color (float blend) {

  color result;

  if (state)
    ::blend_color (off_color, on_color, result, blend);
  else
    ::blend_color (on_color, off_color, result, blend);

  set_color (result);

}

void perpbutton::draw () {
  widget::draw ();
  glBegin (GL_LINES);
    glVertex2i (extents.left, extents.bottom);
    glVertex2i (extents.right, extents.bottom);
    glVertex2i (extents.midx, extents.bottom);
    glVertex2i (extents.midx, extents.top);
  glEnd ();
}

void rotdir::draw () {
  static int pts [16];
  static const int dy = 6;
  static const int vals [] = {1, -1};
  val = vals [state];
  box<int>& e = extents;
  if (state) {
    pts[0]=e.midx; pts[1]=e.top+dy;
    pts[2]=e.right; pts[3]=e.top;
    pts[4]=e.left; pts[5]=e.top;
    pts[6]=e.left; pts[7]=e.bottom;
    pts[8]=e.right; pts[9]=e.bottom;
  } else {
    pts[0]=e.midx; pts[1]=e.top+dy;
    pts[2]=e.left; pts[3]=e.top;
    pts[4]=e.right; pts[5]=e.top;
    pts[6]=e.right; pts[7]=e.bottom;
    pts[8]=e.left; pts[9]=e.bottom;
  }
  glVertexPointer (2, GL_INT, 0, pts);
  glDrawArrays (GL_LINE_STRIP, 0, 5);
  glBegin (GL_LINES);
    glVertex2i (e.midx, e.top-dy);
    glVertex2i (pts[2], pts[3]);
  glEnd ();
}
