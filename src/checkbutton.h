/*
* checkbutton.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __checkbutton
#define __checkbutton

#define DONT_CALL_LISTENER 0

#include "button.h"

struct state_listener;

struct checkbutton : button, click_listener {

	int state; // 0 = off, 1 = on
	state_listener* lsnr;

	int colorize_;
  static color on_color, off_color;

	checkbutton ();
		
	void set_state (int s, int call_listener = 1);
	void turn_on (int call_listener = 1);
	void turn_off (int call_listener = 1);
	void toggle ();

  inline void set_listener (state_listener* sl) {lsnr = sl;}

	void blend_on_off_color (float blend);
	int colorize () const {return colorize_;}
	void colorize (int clr) {colorize_ = clr;}
  void clicked (button& b);

	void set (const std::string& l, state_listener* ls) {
		set_text (l);
		set_listener (ls);
	}

};

struct state_listener : voiddata {
	virtual void changed (checkbutton& cb) = 0;
};

struct perpbutton : checkbutton {
  void draw ();
};

struct rotdir : checkbutton {
  int val;
  void draw ();
};

#define DECL_STATE_LISTENER(name) struct name : state_listener { void changed (checkbutton& cb); };
#define MAKE_STATE_LISTENER(name,var) struct name : state_listener { void changed (checkbutton& cb); }; name var;
#define CLICKED_CHECKBUTTON(scope,name) void scope::name::changed (checkbutton& cb)

#endif
