/*
* chrono.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "chrono.h"
#include "log.h"

#ifdef __BOOST_TIME__
	using namespace boost::gregorian;
	using namespace boost::posix_time;
#endif

audio_clock::audio_clock () {
  secs = delta_secs = 0;
  ticks = delta_ticks = 0;
	dlog << "+++ created audio clock +++" << endl;
}

audio_clock::~audio_clock () {
	dlog << "--- destroyed audio clock ---" << endl;
}

audio_clock& audio_clock::operator++ () {
  ticks += delta_ticks;
  secs += delta_secs;
  return *this;
}

ui_clock::ui_clock () {
	string type;
#ifdef __BOOST_TIME__
	type = "Boost";
#else
	type = "CLOCKS_PER_SEC";
	dlog << "CLOCKS_PER_SEC = " << CLOCKS_PER_SEC << endl;
#endif
  reset ();
	dlog << "+++ created UI clock (" << type << ") +++" << endl;
}

ui_clock::~ui_clock () {
	dlog << "--- destroyed UI clock ---" << endl;
}

void ui_clock::reset () {
#ifdef __BOOST_TIME__
	start = microsec_clock::local_time ();
	elapsed = seconds (0);
#else
	start = clock ();
	elapsed = 0;
#endif
}

double ui_clock::operator() () { // advances clock and returns seconds elapsed from last reset
#ifdef __BOOST_TIME__
  now = microsec_clock::local_time ();
  time_duration delta_elapsed (now - start);
  elapsed += delta_elapsed;
	double delta_secs = delta_elapsed.total_microseconds () / 1000000.0;
  secs_ += delta_secs;
#else
	now = clock ();
	clock_t delta_elapsed = now - start;
	elapsed += delta_elapsed;
	double delta_secs = delta_elapsed * 1.0 / CLOCKS_PER_SEC;
	secs_ += delta_secs;
#endif
	start = now;
	return secs_;
}
