/*
* chrono.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __chrono
#define __chrono

#ifdef __BOOST_TIME__
	#include <boost/date_time/posix_time/posix_time.hpp>
#else
	#include <ctime>
	#include <time.h>
#endif

struct audio_clock {
  double secs;
	double delta_secs;
  double ticks;
  int delta_ticks;
  audio_clock ();
	~audio_clock ();
  audio_clock& operator++ ();
};

struct ui_clock { // uses boost

#ifdef __BOOST_TIME__
	boost::posix_time::ptime start, now; // start time (set at reset), current time
	boost::posix_time::time_duration elapsed;
#else
	clock_t start, now, elapsed;
#endif

  double secs_; // elapsed time since start in seconds
  ui_clock ();
	~ui_clock ();
  void find_elapsed ();
  void reset ();
  void tick ();
  double operator() ();

};

extern ui_clock ui_clk;
extern audio_clock clk;

#endif
