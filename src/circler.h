/*
* circler.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#ifndef __CIRCLER
#define __CIRCLER

#include "spinner.h"
#include "plugin.h"
#include "funktion.h"
#include "constant_radius.h"
#include "sine.h"
#include "cosine.h"
#include "custom_periodic.h"
#include "curve_editor.h"
#include "curve_library.h"
#include "ui_sin_cos_radius.h"

#include <string>

struct circler : plugin, curve_listener, ui_sin_cos_radius_listener {

  // turn regular polygons into shapeforms
  //

  // ui controls for dealing with custom/standard sin, cos and radius
  ui_sin_cos_radius scr;
  void sin_cos_radius_optioned ();
  void sin_cos_radius_edited ();

  point<float> center;

	int num_points;
  float radius;

  float startangle; 
  float endangle;

  float theta;
  float dtheta;
  void render ();

  // ui
  struct {
    spinner<float> radius, startangle, endangle;
    spinner<int> num_points;
  } sp;

  void changed (field& f);

  circler ();
  ~circler ();
  void load_params ();
  void save_params ();
  void setup ();
  //void write ();

};


extern circler circler_;

#endif



