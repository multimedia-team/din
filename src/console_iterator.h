/*
* console_iterator.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __console_iterator
#define __console_iterator

#include <console.h>
#include <string>

struct console_iterator {

  console& con;
  char delim;

  console_iterator (console& c, char delim = ' ');

  console_iterator& operator* ();
  console_iterator& operator++ ();
  console_iterator& operator++ (int);

  template <typename T> console_iterator& operator= (const T& t) {
    con << t;
    return *this;
  }

};

#endif



