/*
* cosine.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __COSINE
#define __COSINE

#include <math.h>
#include "funktion.h"

struct cosine : funktion {
  float operator() (float a, float ea) {
    return cos (a);
  }
};

#endif



