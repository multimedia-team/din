/*
* cross_button.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __cross_button
#define __cross_button

#include "button.h"

struct cross_button : button {
  cross_button (int sz = 8) {set_size (sz);}
  void update () { set_size (size); }
  void draw ();
};

#endif



