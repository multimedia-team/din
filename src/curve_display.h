/*
* curve_display.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __curve_display
#define __curve_display

#include "box.h"
#include "widget.h"

struct multi_curve;
struct curve_editor;

struct curve_display : widget {

  multi_curve* crv;

  box<float> bbox;
  void unit_bbox ();
  void calc_bbox ();
  void setbbox (int sf);
  void add_gutter ();

  void draw ();
  void draw_curve ();

  float* gl_pts;
  int n_glpts;

  curve_display (int w = 96, int h = 96);
  ~curve_display ();

  void set_size (int w, int h) {
    set_extents (extents.left, extents.bottom, extents.left + w, extents.bottom + h);
  }

};

#endif



