/*
* curve_mixer.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __CURVE_MIXER__
#define __CURVE_MIXER__

#include "solver.h"
#include <string>

struct multi_curve;

struct set_mixer {
  std::string name;
	virtual void set_mix (multi_curve& crv, const std::string& nam = "") = 0;
};

struct curve_mixer {

	static float TIME;
	static int SAMPLES;
  static double ELAPSEDT;

  std::string name;

	multi_curve mix;

	int active;

	float a, da;
	void update_a ();

	solver sol;
	float x, dx;

	curve_mixer ();
  ~curve_mixer ();
	void setup (multi_curve& crv, float ix, float idx);
  void fill_alpha (float* alpha, int n);
  void gen_alpha (float* alpha, int n);
	void gen_mix (float* mixb, int n);
	void gen_mix (float* mixb, int n, xhandler& xmin, xhandler& xmax);
	void gen_fm (float* out, int n, float* fm);
	void do_mix (float* out, float* mixb, float* ab, int n);

};
#endif
