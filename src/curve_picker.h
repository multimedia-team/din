/*
* curve_picker.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __curve_picker
#define __curve_picker

#include "label.h"
#include "item_list.h"

struct curve_picker_t : widget, click_listener {

  label title;
  button pick, cancel;
  item_list comps;

  int id;
	int n;

  void setup ();
  int handle_input ();

  void show ();
  void hide ();

  box<int> bg;
  void calc_bg ();

  void draw ();

  void clicked (button& b);

  ~curve_picker_t ();

};

extern curve_picker_t curve_picker;

#endif
