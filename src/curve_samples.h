/*
* curve_samples.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/
#ifndef __curvesamples
#define __curvesamples

struct multi_curve;
struct curve_samples {

  float m; // current displayed sample
  static float dm; // per frame
	static float nsec; // see draw ()

  int n; // max samples
  int n_reached;

  float *x, *xw;
  float *y, *yw;
  float* xp; // for shapeform
  float* xy; // for gl

  float hz;
  float step;

  int nperiods;

	curve_samples ();
	~curve_samples();

  void set (float _hz, int _nperiods = 3);
  void render (multi_curve *crv, curve_editor* ed);
  void draw (curve_editor* ed, multi_curve* crv);
  void towin (curve_editor* ed); // x,y -> xw, yw

};
#endif
