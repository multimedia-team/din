/*
* custom_periodic.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "custom_periodic.h"
#include "console.h"

custom_periodic::custom_periodic (const std::string& name, float _div) : fname (name), crv (fname) {
  sol (&crv);
  div = _div;
}

custom_periodic::~custom_periodic () {
  crv.save (fname);
}

float custom_periodic::operator() (float a, float ea) {
  return sol (a / div);
}

