/*
* custom_periodic.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __CUSTOM_PERIODIC
#define __CUSTOM_PERIODIC

#include "multi_curve.h"
#include "solver.h"
#include "curve_listener.h"
#include "funktion.h"

#include <string>

extern const float TWO_PI;

// periodic function defined by a bezier curve
struct custom_periodic : funktion {
  std::string fname;
  multi_curve crv;
  solver sol;
  float div;
  custom_periodic (const std::string& fname, float _div = TWO_PI);
  ~custom_periodic ();
  float operator() (float a, float ea);
};

#endif



