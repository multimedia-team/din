/*
* defvelaccel.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "defvelaccel.h"

defvelaccel::defvelaccel (const std::string& n) : name (n) {
  dir = VERTICAL;
  neg = 0;
  rndrot = 0;
  clock = anticlock = 0.0f;
  sync = 1;
}

void defvelaccel::setrotrd () {
  rotrd.set (-clock.rad, anticlock.rad);
}

std::istream& operator>> (std::istream& f, defvelaccel& dva) {
  f >> dva.mag >> dva.dir >> dva.neg >> dva.rndrot >> dva.clock >> dva.anticlock >> dva.sync;
  f >> dva.autos.rot.yes >> dva.autos.rot.dir >> dva.autos.rot.mov >> dva.autos.rot.rpm >> dva.autos.rot.dps >> dva.autos.rot.tps;
  f >> dva.autos.rot.uet.deg >> dva.autos.rot.uet.tps;
  f >> dva.autos.flip.yes >> dva.autos.flip.deg;
  f >> dva.autos.pause.yes >> dva.autos.pause.every >> dva.autos.pause.f0r >> dva.autos.pause.tar;
  dva.rotrd.set (-dva.clock.rad, dva.anticlock.rad);
  return f;
}

defvelaccel::autost::pauset::pauset() : yes(1), tar (1), every(1.0f, 0), f0r(1.0f, 0) {}

extern const char spc;

std::ostream& operator<< (std::ostream& f, defvelaccel& dva) {
  f << dva.mag << spc << dva.dir << spc << dva.neg << spc << dva.rndrot << spc << dva.clock << spc << dva.anticlock << spc << dva.sync << spc;
  f << dva.autos.rot.yes << spc << dva.autos.rot.dir << spc << dva.autos.rot.mov << spc << dva.autos.rot.rpm << spc << dva.autos.rot.dps 
  << spc << dva.autos.rot.tps << spc << dva.autos.rot.uet.deg << spc << dva.autos.rot.uet.tps << spc;
  f << dva.autos.flip.yes << spc << dva.autos.flip.deg << spc;
  f << dva.autos.pause.yes << spc << dva.autos.pause.every << spc << dva.autos.pause.f0r << spc << dva.autos.pause.tar;
  return f;
}

std::istream& operator>> (std::istream& f, valt& mag) {
  f >> mag.val >> mag.rndrd >> mag.rd;
  return f;
}

std::ostream& operator<< (std::ostream& f, valt& mag) {
  f << mag.val << spc << mag.rndrd << spc << mag.rd; 
  return f;
}
