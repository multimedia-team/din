/*
* defvelaccel.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __defvelaccel
#define __defvelaccel

#include "angle.h"
#include "random.h"

struct valt {
  float val;
  int rndrd;
  rnd<float> rd;
  valt (float v = 0.0f, int rr = 0) : rd (0.0f, 1.0f) {
    val = v;
    rndrd = rr;
  }
  float operator()(int) {return rd() * val;}
  float operator()() {if (rndrd) return rd() * val; else return val;}
  valt& operator= (float v) {
    val = v;
    return *this;
  }
  operator float () {
    return val;
  }
};

std::istream& operator>> (std::istream&, valt&);
std::ostream& operator<< (std::ostream&, valt&);

struct defvelaccel {

  std::string name;

  valt mag;
  
  int dir;
  enum {HORIZONTAL, VERTICAL, MOUSE};
  int neg;

  int rndrot;
  rnd<float> rotrd;
  void setrotrd ();
  anglet clock, anticlock;
  int sync;

  struct autost {

    struct rott {
      int yes;
      int dir;
      int mov;
      valt rpm, dps, tps;
      struct uett {
        int deg;
        int tps;
        uett () {deg = tps = 0;}
      } uet; // update every tick?

      rott () : rpm(45), dps(6), tps(1) {
        yes = 0;
        dir = 0;
        mov = 0;
      }
    } rot;

    struct flipt {
      valt deg;
      int yes;
      flipt () : deg (180), yes(0) {}
    } flip;

    struct pauset {
      int yes;
      int tar; // target >> 0 = autorotate, 1 = autoflip
      valt every;
      valt f0r;
      pauset ();
    } pause;

  } autos;

  defvelaccel (const std::string& n);

};


std::ostream& operator<< (std::ostream& f, defvelaccel& dva);
std::istream& operator>> (std::istream& f, defvelaccel& dva);



#endif
