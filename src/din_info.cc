/*
* din_info.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "din.h"
#include "console.h"
#include "ui_list.h"
#include "vector2d.h"
#include "defvelaccel.h"

extern string user_data_dir; // user data directory
extern int LEFT, BOTTOM, TOP; // din board extents
extern int HEIGHT; // default number of volumes on the microtonal keyboard
extern int WIDTH; // default number of microtones in a range
extern int TRAILSIZE; // trail size for drone, mondrian ball

void din_info::load () {
  ifstream file ((user_data_dir + "din_info").c_str(), ios::in);
  string ignore;
  if (file) {
    file >> ignore >> BOTTOM >> HEIGHT >> LEFT >> WIDTH; 
    TOP = BOTTOM + HEIGHT; 
    file >> ignore >> delay;
    file >> ignore >> gater;
    file >> ignore >> compress;
    file >> ignore >> voice;
    file >> ignore >> anchor;
		file >> ignore >> vel;
		file >> ignore >> accel;
    file >> ignore >> fader::TIME;
    file >> ignore >> rows >> cols;
    gravity.load (file);
    file >> ignore >> bounce.n >> bounce.speed >> bounce.style;
		file >> ignore >> show_pitch_volume.board >> show_pitch_volume.drones;
		file >> ignore >> drone_rise_time;
    file >> ignore >> drone_fall_time;
		file >> ignore >> snap.style >> snap.left >> snap.right;
		file >> ignore >> sel_range >> mark_sel_range;
		file >> ignore >> set_unset_toggle;
		file >> ignore >> mesh_vars.order >> mesh_vars.point >> mesh_vars.duration >> mesh_vars.sync >> mesh_vars.dpp;
		file >> mesh_vars.apply_to.active >> mesh_vars.apply_to.am >> mesh_vars.apply_to.fm;
		file >> ignore >> drone_pend.n >> drone_pend.depth >> drone_pend.bpm >> drone_pend.orient;
		file >> ignore >> create_this;
		file >> ignore >> dist.pitch >> dist.vol >> dist.pix;
		file >> ignore >> phrasor_right;
		file >> ignore >> seloncre;
		file >> ignore >> drone::IS;
		file >> ignore >> change_note;
		file >> ignore >> change_note_style;
		file >> ignore >> voice_is_voice;
		file >> ignore >> drone::STIFFNESS;
    file >> ignore >> drone::posafxvelt::minmag;
    file >> ignore >> drone::gabt;
    file >> ignore >> cen.x >> cen.y;
    file >> ignore >> wand;
    file >> ignore >> drone::ARE;
    file >> ignore >> drone::LIFETIME;
    file >> ignore >> drone::HANDLESIZE >> TRAILSIZE;
    file >> ignore >> drone::v0;
    file >> ignore >> drone::a0;
    file >> ignore >> menu.dva.which;
    file >> ignore >> drone::chuckt::apt >> drone::chuckt::outline >> drone::chuckt::autoresettrails;
    file >> ignore >> drone::anchored;
    file >> ignore >> drone::arrowt::K >> drone::arrowt::V >> drone::arrowt::U >> drone::arrowt::CAP;
    file >> ignore >> gens;
    file >> ignore >> dpm >> dpl;
    file >> ignore >> revmod;
		dlog << "+++ read din_info +++" << endl;
  }
  cons.last ();
}

void din_info::save () {
  ofstream file ((user_data_dir + "din_info").c_str(), ios::out);
  file << "board " << BOTTOM << spc << HEIGHT << spc << LEFT << spc << WIDTH << endl;
  file << "delay " << delay << endl;
  file << "gater " << gater << endl;
  file << "compress " << compress << endl;
  file << "voice " << voice << endl;
  file << "anchor " << anchor << endl;
	file << "vel " << vel << endl;
	file << "accel " << accel << endl;
  file << "fade_time " << fader::TIME << endl;
  file << "drone_mesh " << rows << spc << cols << endl;
  gravity.save (file);
  file << "bounces " << bounce.n << spc << bounce.speed << spc << bounce.style << endl;
	file << "show_pitch_volume " << show_pitch_volume.board << spc << show_pitch_volume.drones << endl;
	file << "drone_rise_time " << drone_rise_time << endl;
	file << "drone_fall_time " << drone_fall_time << endl;
	file << "drone_snap " << snap.style << spc << snap.left << spc << snap.right << spc << endl;
	file << "selected_range " << sel_range << spc << mark_sel_range << endl; 
	file << "set-unset-toggle " << !set_unset_toggle << endl;
	file << "mesh_vars " << mesh_vars.order << spc << mesh_vars.point << spc << mesh_vars.duration << spc << mesh_vars.sync << spc << mesh_vars.dpp;
	file << spc << mesh_vars.apply_to.active << spc << mesh_vars.apply_to.am << spc << mesh_vars.apply_to.fm <<  endl;
	file << "drone_pendulum " << drone_pend.n << spc << drone_pend.depth << spc << drone_pend.bpm << spc << drone_pend.orient << endl;
	file << "mesh=0__pend=1 " << create_this << endl;
	file << "dist_pitch_vol_pix " << dist.pitch << spc << dist.vol << spc << dist.pix << endl;
	file << "phrasor_right " << MENU.s_phrase_position.extents.right << endl;
	file << "select-drone-on-create " << seloncre << endl;
	file << "drone_is " << drone::IS << endl;
	file << "change_note " << !change_note << endl;
	file << "change_note_style " << change_note_style << endl;
	file << "voice-is-voice " << voice_is_voice << endl;
	file << "connection-stiffness " << drone::STIFFNESS << endl;
  file << "posafxvel-minmag " << drone::posafxvelt::minmag << endl;
  file << "drone-gab-time " << drone::gabt << endl;
  file << "xform-center " << cen.x << spc << cen.y << endl;
  file << "wand " << wand << endl;
  file << "drones-are " << drone::ARE << endl;
  file << "drone-life-time " << drone::LIFETIME << endl;
  file << "handle-trail-size " << drone::HANDLESIZE << spc << TRAILSIZE << endl;
  file << "v0 " << drone::v0 << endl;
  file << "a0 " << drone::a0 << endl;
  file << "def-vel-accel " << MENU.dva.which << endl;
  file << "chuck " << drone::chuckt::apt << spc << MENU.choutline.state << spc << drone::chuckt::autoresettrails << endl;
  file << "anchored " << !drone::anchored << endl;
  file << "drone-arrow " << MENU.dronearrowdefaults.neck() << spc << MENU.dronearrowdefaults.shoulder.width() << spc << MENU.dronearrowdefaults.shoulder.position() << spc << MENU.dronearrowdefaults.cap.state << endl;
  file << "gens " << gens << endl;
  file << "dpm+dpl " << dpm << spc << dpl << endl;
  file << "revmod " << revmod << endl;
	dlog << "+++ saved din_info +++" << endl;
}

#define SEED 6 
void din_info::cent::draw () {
  glColor3f (1, 0.5, 0.5);
  {
    #define SZ (4 * SEED)
    float pts[8] = {x, y-SZ, x, y+SZ, x-SZ, y, x+SZ, y};
    glVertexPointer (2, GL_FLOAT, 0, pts);
    glDrawArrays (GL_LINES, 0, 4);
  }
  {
    float xl = x - SEED, yb = y - SEED, xr = x + SEED, yt = y + SEED; 
    float pts[8] = {xl, yb, xr, yb, xr, yt, xl, yt};
    glVertexPointer (2, GL_FLOAT, 0, pts);
    glDrawArrays (GL_LINE_LOOP, 0, 4);
  }
}

int din_info::cent::handle_input () {
  if (lmb) {
    if (!lmbclk) {
      lmbclk = 1;
      if (op == NONE) {
        box<float> b (x - SEED, y - SEED, x + SEED, y + SEED);
        if (inbox<float> (b, din0.win_mousex, din0.win_mousey)) op = SET; 
      } else op = FINISH;
    }
  } else {
    if (lis && op) lis->gotpoint ();
    if (lmbclk) {
      lmbclk = 0;
      if (op == FINISH) op = NONE;
    }
  }
  return op;
}
