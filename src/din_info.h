/*
* din_info.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "random.h"

struct pointgotlis {
  virtual void gotpoint () = 0;
};

struct din_info {

  gravity_t gravity; // gravity vector

  // applies to microtonal-keyboard only
  // din used to be just the microtonal-keyboard
  //
  struct scroll_t {
    double rept; // key repeat time
    int rate; // scrolls per second
    int dx, dy; // x & y amount per scroll
    scroll_t () : rate (80), dx(15), dy(1) {}
    void calc_repeat_time () {if (rate) rept = 1./ rate;}
  };
  scroll_t scroll;

  int height; // din board height
  int gater; // gater?
  int delay; // delay?
  int compress; // compressor?
  int voice; // lead voice?
  int anchor; // draw drone anchor?
	int vel; // draw velocity vectors of drones?
	int accel; // draw acceleration vectors of drones?

	// drone bounce
	struct bouncet {
		enum {AHEAD, BACK, RANDOM};
		int style;
		int n;
		int speed;
		bouncet () : style (AHEAD), n(1), speed (100) {}
	} bounce;

	enum {DRONE_MESH, DRONE_PENDULUM};
	int create_this;

  // drone mesh
  int rows, cols;
	struct mesh_vars_t {
		int order;
		int point;
		float duration;
		int sync;
		int use_drone_pend;
		int dpp;
		struct _apply_to {
			int active;
			int am;
			int fm;
			void set (int* what, int val) {
				*what = val;
				calc_active ();
			}
			void calc_active () {
				active = am || fm;
			}
			_apply_to () {
				active = am = fm = 0;
			}
		} apply_to;

	} mesh_vars;

	// drone pendulum
	struct drone_pend_t {
		int orient; // 0 - vertical, 1 - horizontal
		int n; // number of drones
		float depth; // am or fm depth depending on orientation
		float bpm; // start bpm of drones
	} drone_pend;

	// show pitch & volume
	struct s_show_pitch_volume {
		int board;
		int drones;
		s_show_pitch_volume () { board = drones = 0;}
	} show_pitch_volume;

	// for drone rise, fall times
  float drone_rise_time, drone_fall_time;

	// drone snapping
	struct snap_t {
		enum {FREE=0, SLIDE, LOCK, MIRROR};
		int style;
		float left, right;
	} snap;

	int set_unset_toggle; // for drone.mod_afx_vel and drone.snap

	// ranges
	int sel_range;
	int mark_sel_range;
	int change_note;
	static const char* cnno [];
	static const char* cnn_opts[];
	int change_note_style;

	// pitch/volume distribution
	struct dist_t {
		int vol;
		int pitch;
		int pix;
		static const int PIX_PER_LEVEL = 5;
		dist_t () : vol(0), pitch(0), pix (PIX_PER_LEVEL) {}
	} dist;

	int phrasor_right; // phrase position slider's right

	int seloncre; 

	int voice_is_voice; // 0 => noise

  // scale/rotate center
  struct cent {

    float x, y;
    void draw ();

    enum {NONE, SET, FINISH};
    int op;
    int lmbclk;

    point<float> h;
    void set (float hx, float hy);
    int handle_input ();
    pointgotlis* lis;

    cent () {
      x = y = 0;
      op = NONE;
      lmbclk = 0;
      lis = 0;
    }

  } cen;

  int wand; // drone wanding?

  struct {
    struct {
      int which;
    } dva;
  } menu;

  int gens;

  int dpm;

  int dpl;

  int revmod;

  void load ();
  void save ();

};

