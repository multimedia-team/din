/*
* drawrrow.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit http://dinisnoise.org/
*/

#include "drawrrow.h"
#include "vector2d.h"

drawrrow::drawrrow () {
  set_extents (0, 0, 100, 100);
  ux = uy = vx = vy = x = y = 0;
  npts = 0;
}

void drawrrow::set_pos (int xx, int yy) {
  widget::set_pos (xx, yy);
  ux = 0;
  uy = extents.top - extents.midy;
  vx = extents.right - extents.midx;
  vy = 0;
  x = extents.midx;
  y = extents.midy;
  calc ();
}

void drawrrow::draw () {
  widget::draw ();
  glColor3f (1, 0.5, 0.5);
  glVertexPointer (2, GL_FLOAT, 0, pts);
  glDrawArrays (GL_LINES, 0, npts);
}
