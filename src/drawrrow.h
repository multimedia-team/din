/*
* drawrrow.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __DRAWRROW
#define __DRAWRROW

#include "widget.h"

struct drawrrow : widget {

  float pts[20];
  int npts;
  int ux, uy;
  int vx, vy;
  int x, y;

  drawrrow ();
  void calc ();
  void set_pos (int xx, int yy);
  void draw ();

};

#endif
