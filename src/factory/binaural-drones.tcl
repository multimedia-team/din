if 0 {
  /*
    * This file is part of DIN Is Noise.
    *
    * DIN Is Noise is copyright (c) 2006 - 2018 Jagannathan Sampath <jag@dinisnoise.org>
    * For more information, please visit http://dinisnoise.org
  */
}

proc after-fade {} {}

proc create-binaural-pair {hz {separation 1} {justification 2}} {
  set vol 0 ;# fade in later
  if {$justification eq 0} { ;# hz to left 
  	set id [binaural-drone create $hz [+ $hz $separation] $vol $justification $separation]
  } elseif {$justification eq 1} { ;# hz to right
  	set just 1 ;# must equal to binaural_drone::RIGHT, see binaural_drone.cc
  	set id [binaural-drone create [- $hz $separation] $hz $vol $justification $separation]
  } elseif {$justification eq 2} { ;# hz at center
  	set separation2 [/ $separation 2.0]
  	set id [binaural-drone create [- $hz $separation2] [+ $hz $separation2] $vol $justification $separation]
  }
  return $id
}

proc after-create n {
  proc after-fade {} "
  	set-text-color 0.5 1 0.5; 
  	echo \"Created $n pairs of binaural drones, [binaural-drone n] pairs in total\"
  	proc after-fade {} {}
  "
}

proc create-binaurals-on-notes {hz {separation 1} {intervals {1 3 5 8}} {resize_separation 1}} {

  set n 0
  set justification [get-var binaural_drones.justification]

  foreach i $intervals {
  	global $i
    set l_resize_separation "$separation [expr $$i*$separation]"
  	lappend created [create-binaural-pair [expr $hz*$$i] [lindex $l_resize_separation $resize_separation] $justification]
  	incr n
  }

  binaural-drone update-list

  set-these-binaurals-volume $created [/ [get-var binaural_drones.master_volume] [binaural-drone n]]

  after-create $n

}

proc create-binaurals-from-pitch {hz {separation 1} {pairs 1} spacing} {

  set-var binaural_drones.starting_pitch $hz
  set-var binaural_drones.spacing $spacing
  set justification [get-var binaural_drones.justification]

  for {set i 0} {$i < $pairs} {incr i} {
    lappend created [create-binaural-pair $hz $separation $justification]
  	set hz [+ $hz $spacing]
  }

  binaural-drone update-list

  set-these-binaurals-volume $created [/ [get-var binaural_drones.master_volume] [binaural-drone n]]

  after-create $pairs
}

proc set-all-binaurals-volume {{max 1}} {
  set n [binaural-drone n]
  if {$n eq 0} return
  set v [expr $max * 1.0/$n]
  for {set i 0} {$i < $n} {incr i} { binaural-drone edit $i v $v}
  proc after-fade {} {}
}

proc wrap-get-nums {s e i} {
  set r [get-nums $s $e $i]
  return "[llength $r] $r"
}

proc filter {in op args} {
  set i 0
  set outi {}
  set outv {}
  if {$op eq "<>"} {
  	set lo [lindex $args 0]
  	set hi [lindex $args 1]
  	foreach v $in {
  		set e [expr ($v >= $lo) && ($v <= $hi)]
  		if $e {
  			lappend outv $v
  			lappend outi $i
  		}
  		incr i
  	}
  } else {
  	set value [lindex $args 0]
  	foreach v $in {
  		set e [$op $v $value]
  		if $e {
  			lappend outv $v
  			lappend outi $i
  		}
  		incr i
  	}
  }
  return [list $outi $outv]
}

proc filter-binaurals {what op {v1 0} {v2 0} {v3 1}} {
  set filtered [lindex [filter [binaural-drone get $what] $op $v1 $v2] 0]
  return "[llength $filtered] $filtered"
}

proc set-these-binaurals-volume {which v} {
  set nw [llength $which]
  if $nw {
  	set nv [llength $v]
  	if {$nv eq 1} {
  		foreach i $which {binaural-drone edit $i v $v}
  	} else {
  		for {set i 0} {$i < $nv} {incr i} {
  			binaural-drone edit [lindex $which $i] v [lindex $v $i]
  		}
  	}
  	proc after-fade {} {}
  } else {
  	set-text-color 1 0.5 0.5; 
  	echo "Please select some binaural drone pairs"
  }
}

proc set-selected-binaurals-volume {v} {
  set-these-binaurals-volume [binaural-drone get selected] $v
}

proc delete-selected-binaurals {} {
  global g_which;
  set g_which [binaural-drone get selected]
  set-these-binaurals-volume $g_which 0
  proc after-fade {} {
  	global g_which
  	set d 0
  	foreach i $g_which {
  		set j [- $i $d]
  		binaural-drone delete $j
  		incr d
  	}
  	echo "Deleted $d pairs of binaural drones"
  	binaural-drone update-list
  	binaural-drone update-selection
  	proc after-fade {} {}
  }
}

proc sync-selected-binaurals {} {
  global g_which g_vol
  set g_which [binaural-drone get selected]
  set g_vol [binaural-drone get selected_volumes [llength $g_which] $g_which]
  set-these-binaurals-volume $g_which 0
  proc after-fade {} {
  	global g_which g_vol
  	binaural-drone sync [llength $g_which] $g_which
  	set-these-binaurals-volume $g_which $g_vol
  	proc after-fade {} {
  		global g_which
  		echo "Synced [llength $g_which] binaural drone pairs"
  		proc after-fade {} {}
  	}
  }
}

proc modulate-selected-binaurals {amount} {
  global g_which
  set g_which [binaural-drone get selected]
  if {[llength $g_which]} {
  	foreach i $g_which { binaural-drone edit $i modulate $amount }
  	proc after-fade {} {
  		global g_which
  		echo "Modulated [llength $g_which] binaural drone pairs"
  		binaural-drone update-selection
  		proc after-fade {} {}
  	}
  } else {
  	echo "Please select some binaural drone pairs";
  }
}

proc flip-selected-binaurals {} { ;# done in c++, here to update selection after fade
  proc after-fade {} {
  	binaural-drone update-selection
  	proc after-fade {} {}
  }
}
