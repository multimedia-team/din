set get-beat(name) get-beat
set get-beat(short) gbt
set get-beat(purpose) {get beat info of components}
set get-beat(invoke) {get-beat OR gbt <list of components> [what]}
set get-beat(help) {component name can be:
  fm - frequency modulater
  am - amplitude modulater
  os - octave shifter
  gr - gater
  what can be:
    all
    first
    last}
set get-beat(examples) {get-beat gr ;# return current beat the gater
gbt {fm am} ;# get beat of fm & am
get-beat gr all ;# return current beat, first beat & last beat of the gater
{0.5 0 1} ;# sample response}
