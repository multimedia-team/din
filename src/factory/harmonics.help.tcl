set harmonics(name) N/A
set harmonics(short) N/A
set harmonics(purpose) {1. Assign MIDI sliders 1-20 to the 20 harmonics.
2. Automagically change the harmonics of the Sine Mixer plugin.}
set harmonics(invoke) {load-patch harmonics ;# loads this patch
  
  MIDI sliders or knobs wth CC 1 to 20 are assigned to the 20 harmonics.
  You must set these CC numbers with the software that comes with your 
  MIDI controller.
  
  For automatic harmonics change,

  set variable harmonics to pick the harmonics of the Sine Mixer 
  to change.  There are 20 harmonics in the Sine Mixer.  To select 
  all the harmonics:

  set harmonics [get-nums 0 19]

  get-nums is a built-in command that returns a list of numbers, 
  0 to 19 in this case.

  set harmonics [get-nums 0 19 2] ;# even number harmonics
  set harmonics [get-nums 1 19 2] ;# odd number harmonics

  set variable duration to set the amount of time that should pass
  before the harmonics change.

  set duration 0.1 ;# animate harmonics every 1/10th of a second

  ;# animate harmonics in sync with frame rate of DIN
  set duration [/ 1.0 [get-var fps]]

  ;# amount by which selected harmonics update every duration
  set amount 0.01 

  set anim 1 ;# starts animating the harmonics every duration
  ;# do this in the curve editor where you want the effect

  set anim 0 ;# stops animating

  ;# utils
  
  ;# randomise harmonics with values 0 to 1
  randomise-harmonics $harmonics

  ;# randomise harmonics with values 0.25 to 0.75
  randomise-harmonics $harmonics 0.25 0.75}

set harmonics(help) {}
set harmonics(examples) {}
