if 0 {
  /*
    * This file is part of DIN Is Noise.
    *
    * DIN Is Noise is copyright (c) 2006 - 2023 Jagannathan Sampath <jag@dinisnoise.org>
    * For more information, please visit http://dinisnoise.org
    *
    *
  */
}

set max_harmonics 20 ;# 20 harmonics of the sine mixer

;# harmonics that animate
set harmonics [get-nums 0 19] ;# get first 20 harmonics of sine mixer

# set harmonics [get-nums 0 19 2] ;# even number harmonics
# set harmonics [get-nums 1 19 2] ;# odd number harmonics

set duration 0.025 ;# animate harmonics every duration [in seconds]

proc animater {args} {

  global anim harmonics start timenow amount_ amount editor

  if $anim {

  	if {[get-var is_curve_editor]} {

  		set editor [get-var curve_editor_id]

  		;# make loop function to handle animation
  		proc loop {} {
  			global timenow start duration amount_ harmonics editor not_warned
  			if {($timenow - $start) > $duration} {
  				set now_editor [get-var curve_editor_id]
  				if {$now_editor eq $editor} {
  					foreach i $harmonics {
  						set ai $amount_($i)
  						set ret [change-sine-mixer $i $ai] ;# returns 1 if beyond min/max, 0 otherwise
  						if $ret {set amount_($i) [* -1 $ai]} ;# flip update direction when beyond; its what we do
  					}
  					update-sine-mixer
  				} 					
  				set start $timenow
  			}
  		}
  		set start $timenow
  	} else {
  		set anim 0
  		set-text-color 1 0.5 0.5
  		echo {Please start patch in curve editor}
  	}
  } else {
  	;# resets
  	proc loop {} {} 
  	set amount $amount 
  }
}

trace add variable anim write animater

proc amounter {args} {
  global max_harmonics amount amount_
  for {set i 0} {$i < $max_harmonics} {incr i} {
  	set amount_($i) $amount
  }
}
trace add variable amount write amounter

set amount 0.005 ;# amount to move each harmonics every duration
set anim 0

if 0 {
  support for midi sliders and knobs
  their id should be from 1 - 20
}

proc midi-cc {status cc value channel} { 
  global max_harmonics
  set ncc [- $cc 1]
  if {$ncc < $max_harmonics} {
  	set-sine-mixer $ncc [get-val 0 1 $value]
  	update-sine-mixer
  } else {
  	set-text-color 1 0 0
  	echo "MIDI CC must be from 1 to $max_harmonics"
  }
}

;# utils
proc randomise-harmonics {harms {start 0} {end 1}} {
  ;# set list of harmonics to random values between start and end
  set delta [- $end $start]
  foreach i $harms {
  	set-sine-mixer $i [expr $start + rand() * $delta]
  }
  update-sine-mixer
}
