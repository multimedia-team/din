if {0} {

  /*
    * This file is part of din.
    *
    * din is copyright (c) 2006 - 2013 S Jagannathan <jag@dinisnoise.org>
    * For more information, please visit http://dinisnoise.org
    *
  */

}

proc list2var {L} {
  for {set i 0; set j [llength $L]; set k [- $j 1]} {$i < $k} {incr i 2} {
    set var [lindex $L $i]
    set val [lindex $L [+ $i 1]]
    uplevel #0 "set $var $val"
  }
}

proc make-interval-note-vars {} {
  list2var [get-intervals] ;# make intervals into variables eg., set 2b prints 1.05946 (depending on tuning!)
  list2var [get-intervals piano] ;# make piano notes into variables eg., set C prints 261.626
}

make-interval-note-vars
