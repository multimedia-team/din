set midimap(name) N/A
set midimap(short) N/A
set midimap(purpose) {displays MIDI activity on console}
set midimap(invoke) {load-patch midimap}
set midimap(help) {midimap displays midi control change, program change, 
pitch bend & note on & off messages}
set midimap(examples) {}
