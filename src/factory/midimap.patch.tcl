if 0 {
/*
 * This file is part of din.
 *
 * din is copyright (c) 2006 - 2018 Jagannathan Sampath <jag@dinisnoise.org>
 * For more information, please visit http://dinisnoise.org
 *
 *
*/
  displays midi controller slider/knob/buttons/keys & misc others status
}

proc make-tracer {name} {
  proc $name [list status id val ch [list msg $name]] {
    echo "$msg: [format {mesg = %x id = %d value = %d channel = %x} $status $id $val $ch]"
  }
}

proc midimap {} {
  make-tracer midi-note-on
  make-tracer midi-note-off
  make-tracer midi-cc
  global echo_midi
  set echo_midi 1
}

proc midi-program-change {status value} {
  echo "midi-program-change; status = $status, value = $value"
}
proc midi-pitch-bend {status ivalue fvalue} {
  echo "midi-pitch-bend, status = $status, ivalue = $ivalue, fvalue = $fvalue"
}

proc list-bpms {} {
  return {gr fm am os}
}

proc midi-clock {} {
  global midibpm
  set-bpm [list-bpms] $midibpm
  global echo_midi
  echo "MIDI bpm: $midibpm"
}

proc midi-start {} {
  foreach i [list-bpms] {
  	set-beat $i [get-beat $i first]
  }
  global echo_midi
  echo "MIDI start"
}

proc unload_midimap {} {

  set procs {midi-note-on midi-note-off midi-cc midi-clock midi-start midi-program-change midi-pitch-bend}

  foreach i $procs {
  	proc $i {args} {}
  }

  global echo_midi
  set echo_midi 0

}

midimap
