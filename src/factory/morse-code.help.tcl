set morse-code(name) morse-code
set morse-code(short) mc
set morse-code(purpose) {convert text to bezier curve pattern using bezier curves 
patterns of morse code primitives: dot, dash, inner letter spacing, letter
spacing and word spacing}
set morse-code(invoke) {morse-code OR mc <text> [nbeats]}
set morse-code(help) {text can contain letters & numbers only.

if nbeats is available the generated bezier curve pattern is quantised
such that the x of last vertex = nbeats.  if nbeats is not available,
the generated bezier curve pattern is quantised to 1 beat.

the generated bezier curve pattern is placed into the copy curve.
paste into any curve in any curve editor (press LCTRL + v).}
set morse-code(examples) {morse-code sos ;# generate bezier curve pattern and place in copy curve
morse-code sos 3 ;# generate bezier curve pattern quantised to 3 beats
morse-code {a long piece of text} ;# just a longer piece of text}
