set notation(name) notation
set notation(short) no
set notation(purpose) {set display notation of notes on the din board}
set notation(invoke) {notation <what>}
set notation(help) {what is:
western OR w
numeric OR n}
set notation(examples) {
notation w ;# display western notation
notation n ;# display numeric notation
no western ;# display western notation
no numeric ;# display numeric notation
notation ;# prints current notation
;# useful in tandem with notation command:
set-var show_cursor_info 1 ;# displays frequencies of notes & volume under mouse cursor}
