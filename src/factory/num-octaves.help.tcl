set num-octaves(name) num-octaves
set num-octaves(short) noct
set num-octaves(purpose) {set the number of octaves of the current scale}
set num-octaves(invoke) {num-octaves OR noct <number>}
set num-octaves(help) {always preserves existing drones' position but may remap
their pitches to available tone ranges.
preserves range sizes}
set num-octaves(examples) {num-octaves 4 ;# span current scale over 4 octaves
num-octaves 128 ;# span current scale over 128 octaves
num-octaves 0 ;# reset to default of 1 octave}
