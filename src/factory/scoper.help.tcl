set scoper(name) N/A
set scoper(short) N/A
set scoper(purpose) {change colors of left & right channels of oscilloscope}
set scoper(invoke) {load-patch scoper}
set scoper(help) {slider 1, 2 & 3 change red, green & blue components of left channel
slider 4, 5 & 6 change red, green & blue components of right channel
edit user/scoper.patch.tcl to change slider ids}
set scoper(examples) {}
