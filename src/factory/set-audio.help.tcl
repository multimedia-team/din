set set-audio(name) set-audio
set set-audio(short) sa
set set-audio(purpose) {set parameters of an audio output device}
set set-audio(invoke) {set-audio <parameter> <value>}
set set-audio(help) {parameter can be:
device OR d
sample_rate OR sr
samples_per_channel OR   
device number can be found using command list-audio 
samples_per_channel must be powers of 2. eg. 128, 256, 512, 1024}
set set-audio(examples) {set-audio device 1 
set-audio sample_rate 48000
set-audio samples_per_channel 256}
