set set-bpm(name) set-bpm
set set-bpm(short) sb
set set-bpm(purpose) {set bpm of a component}
set set-bpm(invoke) {set-bpm OR sb <list of component names> <list of bpms>}
set set-bpm(help) {component name can be:
gr = gater
am = voice amplitude modulater
fm = voice frequency modulater
os = octave shifter}
set set-bpm(examples) {set-bpm gr 10 ;# set gater bpm to 10
sb gr 10;# ditto but short form
set-bpm {fm am} {120 240} ;# set fm bpm to 120, am bpm to 240}
