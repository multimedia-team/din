set-bpm fm 90 
set-bpm am 90 
set-bpm gr 90 
set-bpm os 90 
set-style fm loop 
set-style am loop 
set-style gr loop 
set-var am_depth 0
set-var fm_depth 0
set-delay left 1000.00
set-delay right 1000.00
set-var version 58.1
notation 1
tuning set equal-temperament
load-scale microtonal_keyboard blues
load-scale keyboard_keyboard blues
load-scale mondrian blues
load-scale binaural_drones major-chord
key microtonal_keyboard 261.626
key keyboard_keyboard 261.626
key mondrian 261.626
key binaural_drones 261.626
set taptarget {gr}
set-var scroll rate 120 x 32 y 8 
set-var zoom rate 120 amount 0.04
set-var pan rate 120 amount 0.03
set-var fps 90
set-var ips 90
set-var drone_wand_dist 20
set-var trail_length 10
set-var voice_volume 0.3
set-var sustain 1
set-var instrument keyboard_keyboard
set-var midi_in 0
set-var stepz 1
set-scope left {0.000000 1.000000 1.000000}
set-scope right {1.000000 1.000000 0.000000}
set-var show_parameters 1
set-kb-layout us
set-var current_plugin 1
set-var mondrian.delta_speed 0.01
set-var mondrian.delta_rotate_velocity 1
set-var mondrian.texture {sd)s}
set-var mondrian.texstep 1
set-var mixing_time 1
set-var binaural_drones.master_volume 0.3
set-var binaural_drones.justification 2
set-var curve-samples-nsec 1
array set microtonal_keyboard_editors {4 voice-modulation 8 drone-waveform 5 gater 2 microtonal-keyboard-waveform 6 delays 3 drone-modulation 7 octave-shift}
array set keyboard_keyboard_editors {4 keyboard-keyboard-decay 8 morse-code 5 midi-velocity 2 keyboard-keyboard-waveform 6 delays 3 keyboard-keyboard-attack 7 octave-shift}
array set mondrian_editors {4 mondrian-decay 2 mondrian-waveform 6 delays 3 mondrian-attack 7 octave-shift}
array set binaural_drones_editors {2 binaural-drones-waveform}
