if 0 {
  /*
  	* This file is part of din.
  	*
  	* din is copyright (c) 2006 - 2022 S Jagannathan <jag@dinisnoise.org>
  	* For more information, please visit http://dinisnoise.org
  	*
  	*
  */
}

proc make-user-dir {{prefix ""}} {
  if {$prefix ne {}} {
    set _user ~/.din
  	set _factory $prefix/share/din/factory
  } else {
    set _user user
    set _factory factory
  }

  set _user_exists [file exists $_user]
  set factory_reset [file exists $_user/reset2factory]
  if $_user_exists {
    if $factory_reset {
      file delete -force $_user
      file copy $_factory $_user
    }
  } else {
    file copy $_factory $_user
  }

  global factory user
  set factory $_factory
  set user $_user

}

;# short for sourcing tcl scripts / patches
proc src {name} {
  global user
  uplevel #0 source $user/$name.tcl
}
