set start-end(name) N/A
set start-end(short) N/A
set start-end(purpose) {For each assigned MIDI #CC, go from a start value to an end value and
run a command for each inbetween value.

can be used to set key, set bpm, set AM and FM depth, set drone master
volumes & more.}
set start-end(invoke) {load-patch start-end ;# loads this patch}
set start-end(help) {}
set start-end(examples) {assign ;# no arguments -> just print current assignments

;# assign MIDI #CC ie slider/knob 1 to go from current key to an octave above
assign 1 [key value] [expr $8 * [key value]] key

;# assign MIDI #CC 2 to go from current key to 2 octaves above
assign 2 [key value] [expr $8 * $8 * [key value]] key

;# assign MIDI #CC 3 to go from current key to perfect-fourth an octave above
assign 3 [key value] [expr $8 * $4 * [key value]] key

;# assign MIDI #CC 4 to go from BPM of gater to 4 times BPM of gater
assign 4 [get-bpm gr] [expr 4 * [get-bpm gr]] {set-bpm gr}

8 and 4 are built in interval variables in din with values determined
by the current tuning ($8 & $4 substitute their values ie 2 & 1.33333
respectively. see TCL documentation).

;# must use {} to separate & include words properly.
;# see TCL documentation

;# assign MIDI #CC 5 to set fm_depth variable from 0 to 100.

assign 5 0 100 set-var fm_depth ;# wrong :(

assign 5 0 100 {set-var fm_depth} ;# right :)

;# retain start/end as above ie 0 & 100 but change command to set-bpm gr
assign 5 . . {set-bpm gr}}
