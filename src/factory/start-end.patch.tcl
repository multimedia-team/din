if {0} {

  /*
   * This file is part of din.
   *
   * din is copyright (c) 2006 - 2012 S Jagannathan <jag@dinisnoise.org>
   * For more information, please visit http://dinisnoise.org
   *
  */

  for each assigned midi #cc, go from a start value to an end value
  executing a command for each inbetween value

  can be used to:

    set AM and FM depth
    set bpm
    set drone master volume
    set key & more

  examples:

    assign ;# no arguments -> just print current assignments

    ;# assign MIDI #CC ie slider/knob 1 to go from current key to an octave above
    assign 1 [key value] [expr $8 * [key value]] key

    ;# assign MIDI #CC 2 to go from current key to 2 octaves above
    assign 2 [key value] [expr $8 * $8 * [key value]] key

    ;# assign MIDI #CC 3 to go from current key to perfect-fourth an octave above
    assign 3 [key value] [expr $8 * $4 * [key value]] key

    ;# assign MIDI #CC 4 to go from BPM of gater-l to 4 times BPM of gater-l
    assign 4 [get-bpm gr] [expr 4 * [get-bpm gr]] {set-bpm gr}

    8 and 4 are built in interval variables in din ($8 & $4 are their
    values -> see TCL documentation).

    ;# must use {} to separate & include words properly.
    ;# see TCL documentation

    assign 5 0 100 set-var fm_depth ;# wrong :(

    ;# assign MIDI #CC 5 to set fm_depth variable from 0 to 100.
    assign 5 0 100 {set-var fm_depth} ;# right :)

    ;# retain start/end as above ie 0 & 100 but change command to set-bpm gr
    assign 5 . . {set-bpm gr}

}

proc assign {{cc ""} {startt .} {endd .} {cmdd .}} {

  global start end cmd

  if {$cc eq ""} {
    ;# print all assignments
    foreach i [lsort [array names start]] {
      echo "#cc: $i, start: $start($i), end: $end($i), command: $cmd($i)"
    }
    return
  }

  if {$startt ne "."} {
    ;# set start
    set start($cc) $startt
  }

  if {$endd ne "."} {
    ;# set end
    set end($cc) $endd
  }

  if {$cmdd ne "."} {
    ;# set command
    set cmd($cc) $cmdd
  }

}

proc exists {cc} {
  ;# does cc exist ie has cc got an assignment?
  global start
  lsearch [array names start] $cc
}

proc midi-cc {status cc value channel} {
  global start end cmd
  if {[exists $cc] > -1} {
    set s $start($cc) ;# get start
    set e $end($cc) ;# get end
    set c $cmd($cc) ;# get command
    ;# execute command with interpolated value as only argument
    eval "$c [get-val $s $e $value]"
  }
}
