set tuning(name) tuning
set tuning(invoke) {tuning list OR set OR get [name]}
set tuning(purpose) {change the tuning of din}
set tuning(help) {tuning list - lists all available tunings
tuning set <name> - sets the current tuning to name
tuning get - gets the current tuning}
set tuning(examples) {tuning list ;# lists all available tunings
tuning set et ;# sets din to equal temperament tuning
tuning set ji ;# sets din to just intonation tuning
tuning set pytha ;# sets din to pythogoras tuning}
