/*
* file-utils.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/
#ifndef __fileutils
#define __fileutils

#include <fstream>
#include <string>

extern std::string user_data_dir;

struct file_in {
	std::ifstream f;
	int opened;
	file_in (const std::string& fn) : opened (0) {
		f.open ((user_data_dir + fn).c_str(), std::ios::in);
		opened = !f.fail ();
	}
	ifstream& operator() () {return f;}
};

struct file_out {
	std::ofstream f;
	file_out (const std::string& fn) {
		f.open ((user_data_dir + fn).c_str(), std::ios::out);
	}
	ofstream& operator() () {return f;}
};
#endif
