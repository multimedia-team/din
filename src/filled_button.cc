/*
* filled_button.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/
#include "filled_button.h"
void filled_button::draw () {
  draw_bbox ();
}
