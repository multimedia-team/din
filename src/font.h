/*
* font.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef _FONT
#define _FONT

#include <string>
#include <fstream>
#include <map>

#include "dingl.h"
#include "glyph.h"

#define MAKETYPE(N, T, U, V) \
struct N { \
  T U;\
  T V;\
  N () { U = 0; V = 0;}\
};

MAKETYPE (charwidtht, int, max, avg)
MAKETYPE (charheightt, int, max, avg)
MAKETYPE (cellsizet, int, x, y)
MAKETYPE (spacingt, int, ch, word)

struct font {

  std::string fname;
  std::string name;

  int nchars;

  charwidtht charwidth;
  charheightt charheight;
  cellsizet cellsize;
  spacingt spacing;
  
  int lift;
  int headroom; // space above char

  std::map <char, glyph> characters; // vector desc of chars
  std::map < char, std::map<char, int> > kern; // char-char kerning

  int mod;

  font (const std::string& fn);
  ~font ();
  void load (const std::string& fn);
  void load (std::ifstream& file);
  void save ();
  const std::string& filename () const { return fname;}

  int char_width (char c);
  int char_height (char c);
  void calc_line_height ();

  void draw_char (char c, int x, int y, int z = 0);

  const std::map<char, glyph>& get_chars ();
  void set_chars (const std::map<char, glyph>& chars);

#ifdef __SVG_OUT__
  std::ofstream svg;
  void write_char (char c, int x, int y, int z = 0);
#endif

#ifdef __PLOTTER_OUT__
	std::ofstream hpgl;
	void plot_char (char c, int x, int y, int z = 0);
#endif

};

extern font fnt;
extern int line_height;

inline void draw_char (char c, int x, int y, int z = 0) {
  fnt.draw_char (c, x, y, z);
}

int draw_string (const std::string& s, int x, int y, int z = 0);

#ifdef __SVG_OUT__
	int write_string (const std::string& s, int x, int y, int z = 0);
#endif

#ifdef __PLOTTER_OUT__
	int plot_string (const std::string& s, int x, int y, int z = 0);
#endif

int get_char_width (const std::string& s);
int get_char_height (const std::string& s);
void get_char_width_height (const std::string& s, int& w, int& h);

#endif
