/*
* fractaliser.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#ifndef __FRACTALISER
#define __FRACTALISER

#include "spinner.h"
#include "curve_editor.h"
#include "curve_display.h"
#include "curve_library.h"
#include "plugin.h"
#include <list>

struct fractaliser : plugin {

  multi_curve seed; // apply seed curve on input curve
  curve_editor ed;
  curve_listener lis;
  curve_library lib;
  curve_editor* back_ed;
  float threshold;
  std::list < point<float> > left_tangents, right_tangents; // vertices are in plugin.h/cc

  label l_seed;
  arrow_button ab_left, ab_right;
  curve_display cd_seed;
  spinner<float> sp_threshold;
	spinner<float> sp_scale;
  button b_edit;
  button b_mirror;
  void clicked (button& b);

  fractaliser ();
  ~fractaliser ();
  void load_params ();
  void save_params ();

  void setup ();
  void render ();
  int apply (multi_curve& crv);
  void draw (curve_editor* ed);
	void scale_seed_curve (points_array& v, points_array& lt, points_array& rt, int nq, float scale);

};

extern fractaliser fractaliser_;
#endif



