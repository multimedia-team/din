/*
* funktion.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __FUNKTION
#define __FUNKTION

struct funktion {
  virtual float operator() (float ra = 0, float rea = 0) = 0;
};

#endif



