/*
* globals.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#include "globals.h"
#include "viewwin.h"
#include "checkbutton.h"
#include "random.h"
#include "log.h"
#include "curve_editor.h"
#include <map>
#include <iostream>

extern string INTERVALS_FILE;
extern int NUM_INTERVALS;
extern map <string, float> INTERVALS;
extern vector<string> INTERVAL_NAMES;
extern vector<float> INTERVAL_VALUES;
extern map <string, int> NOTE_POS;
extern int VAR_MIN, VAR_MAX;
extern string VARSTR;
extern int VAR_MIN2, VAR_MAX2;
extern std::string VARSTR2;
extern char BUFFER [];
extern const char* INDIAN_SWAR [];
extern map<string, string> INT2IND;

extern string user_data_dir;

int load_globals::load_intervals (const string& fname) {

  ifstream file ((user_data_dir + fname).c_str(), ios::in);
  if (file) {
    INTERVALS_FILE = fname;

    file >> ignore >> NUM_INTERVALS;

    INTERVALS.clear ();
    INTERVAL_NAMES.clear ();
    INTERVAL_VALUES.clear ();

		string interval_name;
		float value;
    for (int i = 0, pos = 0; i < NUM_INTERVALS; ++i) {
      file >> interval_name >> value;
      INTERVALS [interval_name] = value;
      NOTE_POS [interval_name] = pos++;
      INTERVAL_NAMES.push_back (interval_name);
      INTERVAL_VALUES.push_back (value);
      INT2IND[interval_name] = INDIAN_SWAR[i];
			// dlog << interval_name << spc << INDIAN_SWAR[i] << endl;
    }

    return 1;
  } else return 0;

}

load_globals::load_globals () {
  string fname (user_data_dir + "globals");
  ifstream file (fname.c_str(), ios::in);
  if (file) {

    file >> ignore >> INTERVALS_FILE;

    file >> ignore >> viewport::handle_radius;

		file >> ignore >> widget::R >> widget::G >> widget::B;
		file >> ignore >> checkbutton::on_color.r >> checkbutton::on_color.g >> checkbutton::on_color.b; 
		file >> ignore >> checkbutton::off_color.r >> checkbutton::off_color.g >> checkbutton::off_color.b;
		file >> ignore >> curve_editor::vtxlbl.r >> curve_editor::vtxlbl.g >> curve_editor::vtxlbl.b;

		extern rnd<int> RAN_MOD_BPM;
		float rmb1, rmb2;
		file >> ignore >> rmb1 >> rmb2; RAN_MOD_BPM = rnd<int> (rmb1, rmb2);

		file >> ignore >> VAR_MIN >> VAR_MAX;
		sprintf (BUFFER, "%d %d", VAR_MIN, VAR_MAX); 
		VARSTR = BUFFER;

    file >> ignore >> VAR_MIN2 >> VAR_MAX2;
    sprintf (BUFFER, "%d %d", VAR_MIN2, VAR_MAX2); 
    VARSTR2 = BUFFER;

    load_intervals (INTERVALS_FILE);
		dlog << "loaded globals from: " << fname << endl;
  } else {
    dlog << "!!! could not load globals !!!" << endl;
  }
}
