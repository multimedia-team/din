/*
* globals.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#ifndef __globals
#define __globals

#include "tokenizer.h"

#include <cstring>
#include <string>

struct load_globals {
  std::string ignore;
  int load_intervals (const std::string& fname);
  void load_note_colors (const std::string& fname);
  load_globals ();
};

void calc_volume_vars (int height);
void calc_tonic_limits ();

template <class T> struct pusher {
  virtual T& operator<< (const std::string& s)=0;
  virtual T& operator<< (char c)=0;
};

#include <iostream>

extern const char spc;

template<class T> void into_lines (const std::string& str, pusher<T>& push) {

  static tokenizer tz;
  static const char endln = '\n';

  if (str.length()) {
    tz.str (str);
    tokenizer::delim = endln;
    std::string ln;
    while (1) {
      tz >> ln;
      if (ln == "") break; else { push << ln << endln; }
    }
    tokenizer::delim = spc;
  }
}

#endif



