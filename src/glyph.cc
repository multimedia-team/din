/*
* glyph.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "glyph.h"

#include <iostream>
using namespace std;

glyph::glyph (const vector<line>& vl) : lines (vl) {}

void glyph::find_width_height () {

	int nlines = lines.size ();
  if (nlines) {

		// initialise
		int l, r, t;
		l = r = t = 0;
    vector< point<int> >& points = lines[0].points;
		int npoints = points.size ();
    if (npoints) {
      point<int>& p = points[0];
      l = r = p.x;
      t = p.y;
    }

		// find min/max 
    for (int i = 0; i < nlines; ++i) {
      vector< point<int> >& points = lines[i].points;
      for (int p = 0, q = points.size(); p < q; ++p) {
        point<int>& pt = points[p];
        if (pt.x < l) l = pt.x; else if (pt.x > r) r = pt.x;
        if (pt.y > t) t = pt.y;
      }
    }

    width = r - l;
    height = t;
    
  }

}



