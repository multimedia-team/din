/*
* glyph.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __glyph
#define __glyph

#include <vector>
#include "line.h"

struct glyph {

  std::vector<line> lines;
  int nln;
  int width, height;

  glyph (const std::vector<line>& vl);
  glyph (int w = 3, int h = 8) {width = w; height = h;}

  void find_width_height ();
};

#endif



