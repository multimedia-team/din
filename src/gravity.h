/*
* gravity.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __GRAVITY
#define __GRAVITY

#include "widget.h"
#include "arrow_button.h"
#include "checkbutton.h"
#include "spinner2.h"
#include "point.h"
#include "multi_curve.h"
#include "mod_params.h"
#include "curve_editor.h"
#include "ui_list.h"
#include "options_list.h"

#define DONTCALCMAG 0

struct drone;

struct gravity_t : widget, click_listener, mouse_slider_listener {

  label lbl;
  arrow_button fold, left, right, up, down;
  button zero, here, size;
  button base2manual, base2drone;
  button tip2manual, tip2drone;
  button selbase, seltip, selboth;
  checkbutton cpoint, cmouse;

  DECL_CLICK_LISTENER (manlis)
  DECL_CLICK_LISTENER (drolis)
  manlis b2ml, t2ml;
  drolis b2dl, t2dl;
  MAKE_CLICK_LISTENER (sellis, sell)
  MAKE_CLICK_LISTENER (herelis, herel)
  MAKE_CLICK_LISTENER (presetlis, presetl)
  MAKE_CLICK_LISTENER (sizelis, sizel)

  int lmb_clicked;
  int hitt;
  enum {NEXT_TO_NOTHING = -1, NOTHING, BASE, TIP};
  int hit (const point<float>& p, float mx, float my);

  int handlesize;
  point<float> base, tip;
  point<float> base2tip, p_base2tip;
  point<float> bottomleft, topright;
  point<int> textpos;

  float strength;
  float gx, gy;

  drone* tracked_drone[2];
  int ldwx[2], ldwy[2]; // last tracked drone pos on din window
  void track (int i, float vx, float vy);
  void pointt (float x, float y);
  void chgmag (float amt);

  double mag0, mag;
	int keep_size;

  gravity_t ();
  void load (std::ifstream& file);
  void save (std::ofstream& file);
  int handle_input ();
  int handle_input2 ();

  void set (point<float>& p, float mx, float my, int calc_mag = 1);
  void move (float tox, float toy);
  void calcbase2tip ();
  void calcvisual ();
  void calc (int calc_mag = 1);
  void calcui ();
  void preset (int id);
	int stop_editing ();

  int gl_base [8];
  float gl_arrow[20];

  void draw ();
  void drawrrow ();

  void clicked (button& b);
  void moused (int dir, double scl);

  int unfolded ();

};

#endif
