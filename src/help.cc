/*
* help.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "help.h"
#include "console.h"
#include <fstream>
using namespace std;
extern console cons;
extern string user_data_dir;
help::help (const string& fname) {
  string s;
  ifstream file ((user_data_dir + fname).c_str(), ios::in);
  if (!file) lines.push_back (fname); else while (getline(file, s)) lines.push_back (s);
  dlog << "help file: " << fname << " loaded, num_lines = " << lines.size () << endl;
}
void help::operator() () {
  cons.rollup (0);
  for (int i = 0, j = lines.size(); i < j; ++i) {
    dlog << lines[i] << endl;
    cons << CYAN << lines[i] << eol;
  }
  cons.last ();
}
