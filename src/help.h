/*
* help.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __help_h
#define __help_h
#include <string>
#include <vector>
struct help {
  std::vector<std::string> lines;
  help (const std::string& fname);
  void operator() ();
};
#endif
