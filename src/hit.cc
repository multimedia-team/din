/*
* hit.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "hit.h"
#include "multi_curve.h"
#include "log.h"
using namespace std;

hit_t::hit_t (multi_curve* cv, int cid, int w, int i, int no)  {
  crv = cv;
  crv_id = cid;
  what = w;
  id = i;
	name_only = no;
}

void hit_t::clear () {
  crv = 0;
  crv_id = -1;
  what = NONE;
  id = -1;
	name_only = 0;
}

int hit_t::operator()() const { // hit some curve?
  return (crv_id != -1);
}

int hit_t::operator()(int) const { // hit an existing curve item?
	return (crv_id != -1 && (id > -1) && (id < crv->num_vertices));
}

const point<float>& hit_t::get () {
	points_array* ptr[3] = {&crv->vertices, &crv->left_tangents, &crv->right_tangents};
	points_array& arr = *ptr[what-1];
	return arr[id];
}

hit_t::hit_t (const hit_t& src) {
  copy (src);
}

hit_t& hit_t::operator= (const hit_t& src) {
  copy (src);
  return *this;
}

void hit_t::copy (const hit_t& src) {
  if (&src != this) {
    crv = src.crv;
    crv_id = src.crv_id;
    what = src.what;
    id = src.id;
    left_tangent = src.left_tangent;
    right_tangent = src.right_tangent;
    left_tangent_magnitude = src.left_tangent_magnitude;
    right_tangent_magnitude = src.right_tangent_magnitude;
  }
}

void hit_t::ensure_id () {
	if (id < 0) id = 0;
	else if (id > crv->last_vertex) id = crv->last_vertex;
}
