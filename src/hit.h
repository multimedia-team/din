/*
* hit.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __hit
#define __hit

#include "point.h"
#include "multi_curve.h"

struct multi_curve;
struct hit_t {
  multi_curve* crv; // curve hit
  int crv_id; // id among list of curves
  enum {NONE = 0, VERTEX, LEFT_TANGENT, RIGHT_TANGENT}; // things that can be hit
  int what; // what was hit
  int id; // id of hit thing

	static int name_only;

  // tangent vectors from corresponding vertex
  //
  point<float> left_tangent, right_tangent;
  float left_tangent_magnitude, right_tangent_magnitude;

  hit_t (multi_curve* c = 0, int cid = -1, int w = NONE, int i = -1, int no = 0);
  hit_t (const hit_t& h);
  void clear ();
  int operator()() const; // hit?
	int operator()(int) const; // hit curve item exists?
  int operator== (const hit_t& h) {return ((crv == h.crv) && (what == h.what) && (id == h.id));}
	int matched_id (const hit_t& h) {return ( (crv == h.crv) && (id == h.id) ); }
	void ensure_id ();
  hit_t& operator= (const hit_t& h);
  void copy (const hit_t& src);
  const point<float>& get (); 
};

template <typename T> T& operator<< (T& t, const hit_t& h) {
  if (hit_t::name_only) 
    t << h.crv->name;
  else {
    static const std::string what [] = {"nothing ", "vertex ", "L-tangent ", "R-tangent "};
    static const char* of = " of ";
    t << what[h.what] << h.id << of << h.crv->name;
  }
  return t;
}

#endif
