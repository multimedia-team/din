#include "log.h"
#include "instrument.h"
extern string user_data_dir;

instrument::instrument () {
  tuning = 0;
}

void instrument::load (const string& nf) { 
  fn = user_data_dir + nf;
  ifstream file (fn.c_str(), ios::in);
  if (file) {
    file >> tuning;
  }
  dlog << "+++ loaded " << fn << " +++" << endl;
}

instrument::~instrument () {
  ofstream file (fn.c_str(), ios::out);
  if (file) {
    file << tuning;
  }
  dlog << "+++ saved " << fn << " +++" << endl;
}
