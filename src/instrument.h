/*
* instrument.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __INSTRUMENT
#define __INSTRUMENT

#include "ui.h"
#include "scale_info.h"
#include "octave_shift_data.h"

struct instrument : ui {

	scale_info scaleinfo;
	octave_shift_data osd;

  std::string fn;
  int tuning;

  instrument ();
  ~instrument ();
  void load (const std::string& nf);
	virtual void update_waveform () {}
	virtual void update_attack () {}
	virtual void update_decay () {}
	virtual void load_scale (int dummy = 0) {}

};
#endif



