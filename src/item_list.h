/*
* item_list.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __item_list
#define __item_list

#include "button.h"
#include <vector>
#include <string>

struct tokenizer;

struct item_list;
struct selection_listener {
	virtual void selected (item_list& il, int l) = 0;
};

struct itemt {
  std::string name;
  int sel;
  itemt (const std::string& n, int sell = 0) : name(n), sel(sell) {}
};

struct item_list : button, click_listener {

	int hov, cur, yt;

	int n, n_1, nsel;
	void calc ();

  std::vector <std::string> nums;
  std::vector<itemt> items;

	selection_listener *sel_lis;
	void select (int w);
	void select (int i, int j);
	int select_these (tokenizer& tz);
	void invert_select ();
	std::string get_selected ();
	inline int num_selected () {return nsel;}
	int get_first ();

	item_list ();
  void add (const std::string& i);
	void insert (int i, const std::string& s);
	void remove (int i);
  void clear ();

	void clicked (button& b);

  void set_pos (int x, int y);
  int handle_input ();
  void draw ();
  void update () {}

};
#endif
