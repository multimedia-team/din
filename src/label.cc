/*
* label.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "label.h"
#include "font.h"
#include "viewwin.h"

using namespace std;

label::label (const std::string& t) : text (t) {set_text (text);}

void label::set_text (const string& t) {
	text = t;
  set_name (text);
  int w, h; get_char_width_height (text, w, h);
	const box<int>& e = extents;
	set_extents (e.left, e.bottom, e.left + w, e.bottom + h);
}

void label::draw () {
	widget::draw ();
	draw_string (text, posx, posy);
}

void label::update () {set_text (text);}

void set_label (label& l, const char** lbls, int i) {
	l.set_text (lbls[i]);
}
