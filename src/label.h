/*
* label.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __label
#define __label

#include <string>
#include "widget.h"

struct label : widget {

  std::string text;
  void set_text (const std::string& txt);

	label (const std::string& t = "");

  void update ();
  void draw ();

};

void set_label (label& l, const char** lbls, int i);

#endif
