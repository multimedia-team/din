/*
* label_field.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#ifndef __label_field
#define __label_field

#include "label.h"
#include "field.h"
#include <string>

struct label_field : widget {

  label lbl;
  field fld;

	label_field (int x = 0, int y = 0) {
    lbl.add_child (this);
		lbl.add_child (&fld);
	}

	void update () {
		set_pos (posx, posy);
		lbl.update ();
		fld.update ();
	}

	void set_pos (int x, int y) {
		widget::set_pos (x, y);
		lbl.set_pos (x, y);
		advance_right (x, lbl);
		fld.set_pos (x, y);
		set_extents (lbl.extents.left, lbl.extents.bottom, fld.extents.right, fld.extents.top);
	}

	int handle_input () {
		int t = lbl.handle_input ();
		t |= fld.handle_input ();
		return t;
	}

	void draw () {
		lbl.draw ();
		fld.draw ();
	}

	void set_color (float r, float g, float b) {
		lbl.set_color (r, g, b);
		fld.set_color (r, g, b);
	}

	void set_color (unsigned char r, unsigned char g, unsigned char b) {
		lbl.set_color (r, g, b);
		fld.set_color (r, g, b);
	}

	const color& get_color () const {return lbl.clr;}

  void set_label (const std::string& l) {
    lbl.set_text (l);
		set_pos (posx, posy);
    set_name (l);
  }

  void set_text (const std::string& f) {
    fld.set_text (f);
		set_extents (lbl.extents.left, lbl.extents.bottom, fld.extents.right, fld.extents.top);
  }

  void set_listener (change_listener<field>* l) {
    fld.change_lsnr = l;
  }

	void set_moveable (int m, int mc = 0, int* pmb = &lmb) {
		lbl.set_moveable (m, mc, pmb);
	}
    
};

#endif
