/*
* label_field_slider.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#ifndef __label_field_slider
#define __label_field_slider

#include "font.h"
#include "label.h"
#include "field.h"
#include "slider.h"
#include "filled_button.h"

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

extern font fnt;

template <typename T> struct val_handler {
  virtual void operator() (const T& t) = 0;
};

template <typename T> struct label_field_slider : widget, change_listener< slider<T> >, change_listener<field> {

  filled_button fbtn;
  label lbl;
  field fld;
  slider<T> sld;

  val_handler<T>& vhan;

	label_field_slider (const std::string& t, int ws, int hs, val_handler<T>& vh) : lbl (t), sld (ws, hs), vhan (vh) {
		sld.set_listener (this);
		fld.change_lsnr = this;
		widget* hier [] = {&lbl, &fbtn, &fld, &sld};
		make_hierarchy (hier, 4);
	}

	void update () {
		lbl.update ();
		fld.update ();
		set_pos (posx, posy);
	}

	void set_pos (int x, int y) {

		widget::set_pos (x, y);

		fbtn.set_pos (x, y + fnt.lift);
		advance_right (x, fbtn);

		lbl.set_pos (x, y);
		advance_right (x, lbl);

		sld.set_pos (x, y);
		advance_right (x, sld);

		fld.set_pos (x, y);

	}

	int handle_input () {
		widget* hier [] = {&fbtn, &lbl, &fld, &sld};
		int t = 0;
		for (int i = 0; i < 4; ++i) {
			t = hier[i]->handle_input ();
			if (t) return 1;
		}
		return t;
	}

	void draw () {
		fbtn.draw ();
		lbl.draw ();
		fld.draw ();
		sld.draw ();
	}

	void changed (slider<T>& s) {
		T v = s.get_val ();
		fld.set_text (v);
		vhan (v);
	}

	void changed (field& f) {
		T val = f;
		sld.set_val (val);
		vhan (sld.get_val ());
	}

	/*void set_color (unsigned char r, unsigned char g, unsigned char b) {
		fbtn.set_color (r, g, b);
		lbl.set_color (r, g, b);
		fld.set_color (r, g, b);
		sld.set_color (r, g, b);
	}*/

	const color& get_color () const {return fbtn.clr;}

	void set_limits (T lo, T hi) {
		sld.set_limits (lo, hi);
	}

	void get_limits (T& lo, T& hi) {
		sld.get_limits (lo, hi);
	}

	void set_val (const T& t) {
		sld.set_val (t);
		changed (sld);
	}

	void set_button_listener (click_listener* l) {
		fbtn.set_listener (l);
	}

	void set_label (const std::string& l) {
		lbl.set_text (l);
		set_name (l);
	}

	const std::string& get_text () const {return lbl.text;}
};

#endif
