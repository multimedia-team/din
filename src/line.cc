/*
* line.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#include "line.h"
#include "dingl.h"

void line::draw () {
	int k = 0;
	float r [2] = {0, 1};
	float g [2] = {1, 0};
  glBegin (GL_LINE_STRIP);
    for (int i = 0, j = points.size(); i < j; ++i) {
			glColor3f (r[k], g[k], 0);
			glVertex2i (points[i].x, points[i].y);
			k = !k;
		}
  glEnd ();
}

void line::set (int i, int x, int y) {
  points[i].x = x;
  points[i].y = y;
}

void line::insert (int i, int x, int y) {
  points.insert (points.begin() + i, point<int>(x,y));
}

int line::remove (int i) {
  points.erase (points.begin() + i);
  return points.size();
}
