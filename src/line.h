/*
* line.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __line_h
#define __line_h

#include <vector>
#include "point.h"

struct line {
  std::vector< point<int> > points;
  int npts;
  line () {}
  line (std::vector< point<int> >& pts) : points (pts) {}
  line (std::vector< point<float> >& pts) {
    points.clear ();
    for (unsigned int i = 0, j = pts.size(); i < j; ++i) {
      points.push_back (point<int>((int)pts[i].x, (int)pts[i].y));
    }
  }
  void set (int i, int x, int y);
  void insert (int i, int x, int y);
  int remove (int i);
  void draw ();
};

#endif



