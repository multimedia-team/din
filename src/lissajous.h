/*
* lissajous.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#ifndef __lissajous
#define __lissajous

#include "spinner.h"
#include "plugin.h"

struct lissajous : plugin {

  point<float> center;
  point<float> speed;

	int num_points; 
  
  float theta;
  float dtheta;

  struct {
    struct {
      spinner<float> x, y;
    } speed;
    struct {
      spinner<float> start, end;
    } angle;
    spinner<int> num_points;
  } sp;

  struct {
    float start, end;
  } angle;

  lissajous ();
  ~lissajous ();
  void load_params ();
  void save_params ();

  void setup ();
  void render ();

};

#endif
