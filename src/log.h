/*
* log.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/
#ifndef __LOG
#define __LOG
#include <fstream>
using namespace std;
extern ofstream dlog;
extern const char spc;
#endif
