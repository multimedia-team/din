#ifndef __loom__
#define __loom__

* DIN Is Noise is released under GNU Public License 2.0
#include "widget.h"


struct loom : widget {
	int cell_size;
	int half_cell_size;
	float seam;

	static const int sz = 8;
	int pattern [sz], pattern0 [sz];
	int control [sz];

	loom ();
	void draw ();
	void draw_rect (int x1, int y1, int x2, int y2, float r, float g, float b, int v);
	void flip_pattern ();

};
#endif
