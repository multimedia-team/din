/*
* midi_in.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __midi_in
#define __midi_in

#include "RtMidi.h"
#include <vector>
#include <string>

struct midi_in {

	RtMidiIn rt;

	int num_ports;
	int input_port;
	int available;

  unsigned char channel;
  unsigned char cc;
  unsigned char val;

	std::vector<std::string> names;

	midi_in ();
	void probe ();
	void open ();
	void open (int _input_port);
	void handle_input ();
	std::string get_name (int i) {
		if (i > -1 && i < num_ports) 
      return names[i];
		else 
      return "?";
	}

};

void run_midi_cmd (const std::string& cmd, std::vector<unsigned char>& args);
extern midi_in midiin;

#endif
