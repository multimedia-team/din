/*
* minus_button.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "minus_button.h"

void minus_button::draw () {
	widget::draw ();
	const box<int>& e = extents;
  glBegin (GL_LINES);
    glVertex2i (e.left, e.midy);
    glVertex2i (e.right+1, e.midy);
  glEnd ();
  /*int pts [4]={0};
  glVertexPointer (2, GL_INT, 0, pts);
  pts[0]=e.left; pts[1]=e.midy; pts[2]=e.right; pts[3]=e.midy;
  glDrawArrays (GL_LINES, 0, 2);
  pts[0]=e.right;pts[1]=e.midy;pts[2]=e.left;pts[3]=e.midy;
  glDrawArrays (GL_LINES, 0, 2);*/
}
