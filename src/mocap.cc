/*
* mocap.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "mocap.h"
#include "basic_editor.h"

#include <iostream>
using namespace std;

mocap::mocap () {
  clear ();
}

void mocap::clear () {

  x.clear ();
  y.clear ();

  x.reserve (1);
  y.reserve (1);

  cur = 0;

  state = empty;

}

int mocap::get (float& gx, float& gy, int dir) {

  if (cur >= x.size ())  cur = 0; 
	else if (cur < 0) cur = x.size () - 1;

  gx = x[cur];
  gy = y[cur];
	
	cur += dir;

  return 1;

}

int mocap::add (float wx, float wy) {
  x.push_back (wx);
  y.push_back (wy);
  state = capturing;
  return 1;
}

void mocap::finish (basic_editor* b) {

  for (int i = 0, j = x.size (); i < j; ++i) {
    float wx = x[i], wy = y[i];
    b->win2obj (wx, wy, x[i], y[i]); // win to obj space
  }

  cur = 0;
  state = finished;

}

int mocap::operator() () {
  return (state == finished);
}
