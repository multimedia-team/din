/*
* mod_params.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/
#ifndef __modparams__
#define __modparams__

#include "beat2value.h"
#define NONAME "noname"

struct multi_curve;
struct drone;
struct mod_params { 

  float depth;
	beat2value bv; // rate
	float result;
  float initial; 

  // drone modulation only
  //
  enum {VERTICAL, HORIZONTAL, VELOCITY, ACCELERATION}; // possible modulation directions
  int id; 
  static point<float> vertical, horizontal; // constant directions
  float *dirx, *diry; // current modulation direction
  void setdir (int i, float* x, float* y) {
    id = i;
    dirx = x;
    diry = y;
  }

  void calcdir (drone& d);

  void comconst (multi_curve* c, const std::string& n);
	mod_params (multi_curve* c, int d, point<float>& p, const std::string& n = NONAME);
  mod_params (multi_curve* c, const std::string& n = NONAME);
	void calc (double dt);
	void clear ();

};

#endif
