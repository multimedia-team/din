/*
* modulator.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "multi_curve.h"
#include "modulator.h"
#include "audio.h"
#include "chrono.h"
#include "drone.h"
#include "ui_list.h"

extern const float TWO_PI;
extern const char spc;


void mod_params::comconst (multi_curve* c, const std::string& n) {
	bv.name = n;
	bv.crv = c;
  bv.setup ();
	depth = result = initial = 0;
  id = 0;
  dirx = diry = 0;
}

mod_params::mod_params (multi_curve* c, const std::string& n) {
  comconst (c, n);
}

mod_params::mod_params (multi_curve* c, int i, point<float>& p, const std::string& n) { 
  comconst (c, n);
  setdir (i, &p.x, &p.y);
}

void mod_params::clear () {
  depth = 0;
	result = 0;
	initial = 0;
  bv.set_bpm (bv.bpm);
}

void mod_params::calc (double dt) {
	int dir = 1; if (bv.delta < 0) dir = -1;
  bv.delta = dir * dt * bv.bps;
  result = depth * bv.sol (bv.now, bv.delta, *bv.xmin, *bv.xmax);
}

void mod_params::calcdir (drone& dd) {
  float* px [] = {&vertical.x, &horizontal.x, &dd.vx, &dd.ax};
  float* py [] = {&vertical.y, &horizontal.y, &dd.vy, &dd.ay};
  dirx = px[id];
  diry = py[id];
}
