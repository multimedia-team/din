/*
* modulator.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/
#ifndef __modulator__
#define __modulator__

#include "chrono.h"
#include "mod_params.h"

#include <fstream>
#include <string>

struct multi_curve;

struct modulator {

  enum {AM, FM};

  mod_params am, fm;
  
	enum {FROZEN=-1, NOT_ACTIVE, ACTIVE};
  int active;

	int scrubbed; // used by point_modulator

  double t, dt;

  modulator (multi_curve* fmc, multi_curve* amc) : 
    am (amc, mod_params::VERTICAL, mod_params::vertical), 
    fm (fmc, mod_params::HORIZONTAL, mod_params::horizontal) 
  {
    active = scrubbed = 0;
    t = ui_clk ();
    dt = 0.0;
  }

  void clear () {
		am.clear ();
    fm.clear ();
    active = 0;
  }

  void calc () {

    double now = ui_clk ();
    dt = now - t;
    t = now;

		am.calc (dt);
    fm.calc (dt);

  }

};

#endif
