/*
* morpher.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#ifndef __MORPHER
#define __MORPHER

#include "spinner.h"
#include "plugin.h"
#include "curve_display.h"
#include "solver.h"
#include "slider.h"

// morphs two bezier curves (classic or shapeform)
struct morpher : plugin, change_listener< slider<float> > {

  slider<float> sl_amount;
	slider<float> sl_src_start, sl_tar_start;
  spinner<int> sp_points;

	int num_points;
	float amount;

  struct dat {
    label lbl;
    curve_display cd;
    button get;
    multi_curve crv;
    solver sol;
    std::vector < point<float> > pts;
    int start, cur, total;
    dat () {start = cur = total = 0;}
  } src, tar;

  morpher ();
  ~morpher ();

	void assign_curve (multi_curve& crv, solver& sol, curve_display& crvdis);
	void make_points ();
	void make_classic_pts (std::vector < point<float> >& pts, solver& sol);
	void make_shapeform_pts (std::vector < point<float> >& pts, solver& sol, multi_curve& crv);
	void clicked (button& b);
	void changed (field& f);
	void changed (slider<float>& s);
  void load_params ();
  void save_params ();
  void setup ();
	void render ();
	int apply (multi_curve& crv);

};
#endif
