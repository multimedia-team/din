/*
* morse_code.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __MORSE_CODE
#define __MORSE_CODE

#include "command.h"
#include "multi_curve.h"
#include "curve_editor.h"
#include "listeners.h"

#include <map>
#include <string>

struct morse_code : command { // convert text to morse code to multi_curve to use as a pattern

  // international morse code
  std::map <char, std::string> code;
  int ncodes;

  // bezier curves for morse code primitives
  multi_curve dot, dash;
  multi_curve inner_space, letter_space, word_space;

  // morse code editor
  curve_editor ed;
  morse_code_listener lis;

  // generation
  int add_first_vertex;
  point<float> org;
  point<float> vi;


  morse_code (const std::string& ln, const std::string& sn);
  ~morse_code ();
  int load (const std::string& fname);
  int operator() (tokenizer& tz);
  int create_pattern (const std::string& text, float tox);
  void append (multi_curve& m, multi_curve& p);
  void scale_tox (multi_curve& m, float tox);


};

extern morse_code mc;
#endif



