/*
* multi_curve.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#ifndef __MULTICURVE__
#define __MULTICURVE__

#include <string>
#include <fstream>
#include <vector>

#include "point.h"
#include "curve.h"
#include "box.h"

typedef std::vector< point<float> > points_array;

struct multi_curve {
	
	// multiple bezier curves
	//

	std::vector<curve> curv; // bezier curves of this multi_curve

  // curve components 
  points_array vertices;
  points_array left_tangents;
  points_array right_tangents;
	int num_vertices;
	int last_vertex;

  multi_curve ();
  multi_curve (const std::string& filename);

  void clear (int all = 1); // all => color & name

	// vertex ops
  void add_vertex (float x, float y);
	int insert (float x, float y, float tx, float ty);
	int remove (int i); 
	int set_vertex (int i, float x, float y, int carry_tangents = 0);
	void get_vertex (int i, float& x, float& y);

	// tangent ops
  void add_left_tangent (float x, float y);
  void add_right_tangent (float x, float y);
	void get_left_tangent (int i, float& x, float& y);
	void get_right_tangent (int i, float& x, float& y);
  int set_left_tangent (int i, float x, float y);
  int set_right_tangent (int i, float x, float y);

	float limit; // same for all curves (see curve.h)
	void set_limit (float d);

  int shapeform; // shapeform?
  void setcentroid (int _shapeform);
  void set_shapeform (int yesno);
	void check_shapeform ();
	void get_xy (double d, float& x, float& y); 

  void require_eval (); 
  void evaluate (); // compute latest profile of bezier curves 

	// profile
	void get_profile_points (std::vector<crvpt>& profile); // all curves
	std::vector<crvpt>& get_profile_points (int i); // for ith curve
	int get_total_points ();

	// xform
	float cx, cy; // centroid
	void calc_bbox (box<float>& b);
	void rotate (point<float>& p, float angle);
  void rotate (float angle);
	void scale (point<float>& p, float sx, float sy);
  void scale (float sx, float sy);

	// visual
	float r, g, b; // color of profile
	float rt, gt, bt; // color of tangents
  void set_color (); // random color
  void set_color (float rr, float gg, float bb);
  void get_color (float& rr, float& gg, float& bb) { rr = r; gg = g; bb = b;}
  void calc_tangent_color ();

	// utils
  std::string name; // name
	void set_name (const std::string& name);
	void load (const std::string& filename);
	void load (std::ifstream& file);
	void save (const std::string& filename);
	void save (std::ofstream& file);

};



void create_polyline (multi_curve& crv, const points_array& pts);
void convert2_polyline (multi_curve& crv);
void convert2_catmull_rom (multi_curve& crv, float tangent_size);
multi_curve* get_curve (const std::string& name);
multi_curve* check_list (multi_curve** lst, int n, const std::string& name);

#endif
