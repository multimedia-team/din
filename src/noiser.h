/*
* noiser.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __noiser
#define __noiser

#include "solver.h"
#include "random.h"
#include "beat2value.h"
#include "listeners.h"
#include <fstream>

struct noiser {

	static multi_curve interp;
	static curve_editor ed;
	static noise_interp_lis lis;

	struct value_dat {
		float cur;
		float next;
		float delta;
		value_dat () { cur = next = delta = 0.0f;}
	} value;

  //int setcur, setnext, nextoff;

	float spread;
	rnd<float> random_value;

	struct samples_dat {
		int cur;
		int due;
		int duei;
		float next;
		float err;
		float derr;
		samples_dat () {
			cur = due = duei = 0;
			next = err = derr = 0.0f;
		}
	} samples;


	struct alpha_dat {
		float val;
		float warp;
		float delta;
		alpha_dat () {val = warp = delta = 0.0f;}
	} alpha;
	void calc_delta_alpha ();

	solver warp;

	float last_vol;

	noiser ();
	void operator() (float* L, float* R, int n, float vol);
	void operator() (float* L, float* R, int n, float* vola);
	void set_samples (float s);
	void set_spread (float s);
	void set_value (float v);

};

std::ofstream& operator<< (std::ofstream& o, noiser& n);

#endif
