/*
* note.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#ifndef __NOTE
#define __NOTE

#include <string>
#include <fstream>

struct scale_info;

struct note {

  //
  // musical note / tone
  //

  std::string name;
	std::string hz_name;

	void set_name (const std::string& n);
	void set_name (const std::string& interval, int i);

  float hz; // frequency of note in hertz
  float step; // amount to step in x along the waveform every sample to get the audio level at that sample.

	int scale_pos;
	void change_scale_pos (int j, scale_info& si); 

	int octave;

  note ();
  void set_freq (float f);
	void set_freq (float key, float interval);
	std::ofstream& save (std::ofstream& f);
	std::ifstream& load (std::ifstream& f);

};

#endif



