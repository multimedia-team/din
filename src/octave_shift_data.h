/*
* octave_shift_data.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __OCTAVE_SHIFT_DATA__
#define __OCTAVE_SHIFT_DATA__
struct octave_shift_data {
  float tonic; 
  int dir; // shift direction; 0 = up, 1 = down
  int active; // active?
	int percent_complete; 
	float now;
  octave_shift_data () : tonic(0), dir (-1), active(0), percent_complete(0), now(0) {}
};
#endif



