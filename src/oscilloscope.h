/*
* oscilloscope.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __oscilloscope
#define __oscilloscope

#include "box.h"
#include "ui.h"
#include "arrow_button.h"
#include <string>
#include <vector>

struct oscilloscope_params_t {
	int left, base, height;
	int num_samples;
  float opacity;
	int visible;
	int folded;
	oscilloscope_params_t (int l, int b, int h, int ns, float op, int v, int f);
};

struct oscilloscope : ui, click_listener, widget {

	struct sample_t {
		static float lmin, lmax; // min/max of L
		static float rmin, rmax; // min/max of R
		float left, right; // L and R value  
		sample_t () {left = right = 0;}
	};

  sample_t* samples;
  int num_samples, max_samples, startsamp;
  void alloc (int n);
  void add_samples (float* outl, float* outr, int n);
  void set_num_samples (int n);

	int folded;
	void set_folded (int f);

  int left, base, height;

  oscilloscope (const std::string& _settingsf);
  ~oscilloscope ();

  std::vector<oscilloscope_params_t> params; // for each instrument
  std::string settingsf;
  int load ();
  int save ();
	void load_current_instrument ();
	void save_current_instrument ();

	// gl
  int* vertices;
  float* colors;
  int ndraw;

	// ui
	//

  // channel labels
  int lly, lry, lh;
  static const int buf_size = 64;
  char lbuf1 [buf_size], rbuf1 [buf_size];

	arrow_button fold;
  box<int> win, pick_win;
  int leftx, rightx, endx;
	float lr, lg, lb; // L color
	float rr, rg, rb; // R color

	int lmb_clicked;
	int px, py;
	int move, stop_move;
  int handle_input ();

  void set_height (int h);
  void calc_draw_params ();
  void draw ();
  void show ();
	void clicked (button& b);

  float opacity;
  void setopacity (float o) { opacity = o; }

  int limit;

};
#endif



