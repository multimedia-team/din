/*
* plugin_browser.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "sine_mixer.h"
#include "fractaliser.h"
#include "starrer.h"
#include "spiraler.h"
#include "rose_milker.h"
#include "circler.h"
#include "plugin_browser.h"
#include "lissajous.h"
#include "superformula.h"
#include "ui_list.h"
#include "warper.h"
#include "countries.h"
#include "morpher.h"
#include "number.h"
#include "log.h"
using namespace std;

plugin_browser::~plugin_browser () {
  widget_save ("d_plugin_browser", ctrls, num_ctrls);
}

void plugin_browser::setup () {

  for (int i = 0; i < num_plugins; ++i) {
    plugin* pi = plugins [i];
    add_children_of (pi);
		il_plugins.add (pi->name);
  }

  l_title.set_moveable (1);
  ab_fold.set_listener (this);
  fb_list.set_listener (this);
  il_plugins.set_listener (this);

  cur = 0;
  il_plugins.hide ();

  ctrls[0]=&l_title;ctrls[1]=&ab_fold;ctrls[2]=&fb_list;ctrls[3]=&il_plugins;
  for (int i = 1; i < num_ctrls; ++i) l_title.add_child (ctrls[i]);
  widget_load ("d_plugin_browser", ctrls, num_ctrls);

}

int plugin_browser::handle_input () {
	int r = 0;
  for (int i = 0; i < num_ctrls_1; ++i) {
		r = ctrls[i]->handle_input ();
		if (r) return r;
	}
  if (il_plugins.visible) r = il_plugins.handle_input (); else r = plugins[cur]->handle_input ();
  return r;
}

void plugin_browser::draw () {
  for (int i = 0; i < num_ctrls_1; ++i) ctrls[i]->draw ();
  if (il_plugins.visible) il_plugins.draw (); else plugins[cur]->draw ();
}

void plugin_browser::update () {
	for (int i = 0; i < num_ctrls; ++i) ctrls[i]->update ();
}

void plugin_browser::add_children_of (plugin* p) {
  vector<widget*>& pctrls = p->ctrls;
  for (int i = 0, j = pctrls.size (); i < j; ++i) l_title.add_child (pctrls[i]);
}

void plugin_browser::clicked (button& b) {
  if (&b == &fb_list) {
    if (il_plugins.visible) 
			il_plugins.hide (); 
		else
			il_plugins.show ();
  } else if (&b == &il_plugins) {
		il_plugins.clicked (il_plugins);
		set_cur (il_plugins.cur);
		il_plugins.select (0);
		il_plugins.hide ();
		set_fold (0);
  } else {
    if (folded()) {
			set_fold (0);
    } else {
			set_fold (1);
    }
  }
}

void plugin_browser::set_cur (int c) {
  cur = c;
  plugin* pcur = plugins[cur];
  l_title.set_text (pcur->name);
}

void plugin_browser::set_fold (int f) {
  if (f) {
    ab_fold.set_dir (arrow_button::right);
    plugins[cur]->fold ();
  } else {
    ab_fold.set_dir (arrow_button::down);
    plugins[cur]->unfold ();
  }
	ed->draw_plugin_output = !f;
}

int plugin_browser::folded () {
  return (ab_fold.dir == arrow_button::right);
}

void plugin_browser::draw (curve_editor* ed) {
  if (folded() == 0) plugins[cur]->draw (ed);
}

plugin* plugin_browser::get_cur () {
  return plugins[cur];
}

void plugin_browser::set_ed (curve_editor* e) {
	ed = e;
	for (int i = 0; i < num_plugins; ++i) plugins[i]->set_ed (e);
}
