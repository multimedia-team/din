/*
* plus_button.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "plus_button.h"

void plus_button::draw () {
	widget::draw ();
	const box<int>& e = extents;
  int pts[8]={0};
  pts[0]=e.left;pts[1]=e.midy;
  pts[2]=e.right;pts[3]=e.midy;
  pts[4]=e.midx;pts[5]=e.bottom;
  pts[6]=e.midx;pts[7]=e.top;
	glVertexPointer (2, GL_INT, 0, pts);
  glDrawArrays (GL_LINES, 0, 4);
	pts[0]=e.right;pts[1]=e.midy;
	pts[2]=e.left;pts[3]=e.midy;
	pts[4]=e.midx;pts[5]=e.top;
	pts[6]=e.midx;pts[7]=e.bottom;
	glDrawArrays (GL_LINES, 0, 4);
}






