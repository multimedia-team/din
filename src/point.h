/*
* point.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __point
#define __point

template <class T> struct point {
  T x, y;
  point (T a, T b) : x(a), y(b) {}
  point () : x(0), y(0) {}
  void operator() (T a, T b) {
    x = a;
    y = b;
  }
  bool operator== (const point<T>& p) {
    return ( (x == p.x) && (y == p.y));
  }

	point& operator+= (const T& d) {
		x += d;
		y += d;
		return *this;
	}

	point& operator= (const T& d) {
		x = d;
		y = d;
		return *this;
	}

	point& operator*= (const point<T>& m) {
		x *= m.x;
		y *= m.y;
		return *this;
	}

	point& operator= (const point<T>& q) {
		x = q.x;
		y = q.y;
		return *this;
	}

};

template <typename T> point<T> operator+ (const point<T>& p1, const point<T>& p2) {
	return point<T> (p1.x + p2.x, p1.y + p2.y);
}
#endif
