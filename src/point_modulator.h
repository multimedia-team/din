/*
* point_modulator.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __point_modulator
#define __point_modulator

#include "label.h"
#include "arrow_button.h"
#include "filled_button.h"
#include "checkbutton.h"
#include "cross_button.h"
#include "state_button.h"
#include "modulator.h"
#include "plus_button.h"
#include "utils.h"
#include "spinner.h"
#include "hit.h"

#include <map>

struct mod_dat {
	hit_t hit;
	state_button* sb;
	modulator mod;
	point<float> init;
	mod_dat (hit_t& h, state_button* _sb, float _bpm);
	mod_dat ();
};

struct curve_editor;
struct point_modulator : widget, click_listener, change_listener<field>, state_listener, move_listener {

	curve_editor* ed;

	label title;
	void set_title ();

	int folded;
	arrow_button fold;

	arrow_button play;
	cross_button kill;
	button all, none, invert;
	plus_button plus;

	struct {
		spinner<float> depth;
		spinner<float> bpm;
	} x, y;

	spinner<float> scrub;
	int scrubbed;

	button sync;
	int synced;

	int sbx, sby;

	std::vector<mod_dat> lst;
	int nlst;

	int nsel;
	sel _sel;
	desel _desel;
	togg _togg;
	int multisel;
	void on_lst (const item_op& o);

  void setup (curve_editor* _ed);
  int handle_input ();
	void moved ();
  void draw ();

	state_button* add (hit_t& h);
	void rearrange ();
	void remove (const hit_t& h = hit_t ());
	void ensure_hit_ids ();
	void validate ();

	void clicked (button& b);
	void changed (checkbutton& cb);
	void changed (field& f);

	void bg ();

	static int ref;
	point_modulator ();
  ~point_modulator ();

	void load ();
	void save ();

	int hit (hit_t& h);

	void update_solvers (int i);

	void set_ui ();

};

#endif
