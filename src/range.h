/*
* range.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __range__
#define __range__

#include "box.h"
#include "note.h"
#include "color.h"
#include "modulator.h"
#include <string>
#include <map>
#include <math.h>

struct scale_info;

struct range {

	//
	// microtonal range
	// from one note to another (both included).

  box<int> extents;

  note notes[2]; // end notes
	std::string intervals[2]; // interval names of end notes (see *.tuning files)
	void change_note (int i, int j, scale_info& si);

	int octave;
	void change_octave (int i, int o, scale_info& si);

  float delta_step; // difference of step values ie pitch/frequency of the end notes (see note.h)

  void calc (scale_info& si);
	void calc_note_freq (int i, scale_info& si);

  void draw_labels (int label_what = LEFT, int show_frequency = 0);
  void stamplabel (const string& lbl, int l, int b, int t);

	enum {NONE = -1, LEFT, CENTER, RIGHT, BOTH};

  // for opengl
  int pts [8];

	// modulation
	int fixed; // whats fixed? left, right or center
	modulator mod; // fm => width, am => height
	void init_mod ();

	range ();
	void change_height (int dh);
	void sample_rate_changed ();

	inline float hz_per_pix () {
		float delta_hz = fabs(notes[1].hz - notes[0].hz);
		return (delta_hz * 1.0f / extents.width);
	}
	void print_hz_per_pixel ();

};

void print_range_info (range& ri);

#endif
