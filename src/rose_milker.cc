/*
* rose_milker.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#include "rose_milker.h"
#include "vector2d.h"
#include "ui_list.h"
#include "console.h"
#include <vector>
#include <fstream>
using namespace std;

extern ofstream dlog;

extern const float TWO_PI;
extern const float PI_BY_180;

extern curve_library sin_lib, cos_lib;

rose_milker::rose_milker () : scr (this, "rose_milker.scr", "rose_milker_sin.crv", "rose_milker_cos.crv", "rose_milker_radius.crv", "rose_milker_sin.ed", "rose_milker_cos.ed", "rose_milker_radius.ed", 0) {
  name = "Rose_Milker";
  center.x = 0.5;
  center.y = 0;
  load_params (); 
}

void rose_milker::load_params () {
	ifstream f (make_fname().c_str(), ios::in);
	string ignore;
	f >> ignore >> N >> ignore >> D >> ignore >> num_points >> ignore >> angle.start >> ignore >> angle.end;
}

void rose_milker::save_params () {
	ofstream f (make_fname().c_str(), ios::out);
	string ignore;
	f << "N " << N << endl << "D " << D << endl << "num_points " << num_points << endl << "start_angle " << angle.start << endl << "end_angle " << angle.end << endl;
}

rose_milker::~rose_milker () {
	widget_save ("d_rose_milker", ctrls);
	save_params ();
}

void rose_milker::setup () {

	plugin::setup ();

	widget* _ctrls [] = {&sp.N, &sp.D, &sp.num_points, &sp.angle.start, &sp.angle.end, &scr};
	for (int i = 0; i < 6; ++i) ctrls.push_back (_ctrls[i]);

	num_ctrls = ctrls.size ();

	spinner<int>* spn [] = {&sp.N, &sp.D, &sp.num_points};
	const char* lbl [] = {"N", "D", "Points"};
	int val [] = {N, D, num_points};
	for (int i = 0; i < 3; ++i) {
		spinner<int>* spi = spn[i];
		spi->set (lbl[i], 1, 0, MILLION, this, 0);
		spi->set_value (val[i]);
	}

  sp.angle.start.set ("Start angle", 1.0f, -MILLION, MILLION, this, 0);
  sp.angle.start.set_value (angle.start);

	sp.angle.end.set ("End angle", 1.0f, -MILLION, MILLION, this, 0);
	sp.angle.end.set_value (angle.end);

	widget_load ("d_rose_milker", ctrls);

	scr.setup ();
	scr.set_pos (cb_auto_apply.posx, cb_auto_apply.posy);
	sin_cos_radius_optioned ();

	render ();

}

void rose_milker::render () {

	funktion& f_sin = *scr.pf_sin;
	funktion& f_cos = *scr.pf_cos;

	N = sp.N.f_value; 
	D = sp.D.f_value;
	if (D == 0) return;
	K = N * 1. / D;

	num_points = sp.num_points.f_value;
	if (num_points == 0) return;

  angle.start = sp.angle.start.f_value;
	angle.end = sp.angle.end.f_value;
	float dtheta = (angle.end - angle.start) / num_points * PI_BY_180;
	float theta = angle.start * PI_BY_180;
  float theta_k = theta;
	int j = num_points + 1; 
	points.resize (j);
	float R;
  float ktheta;
	for (int i = 0; i < j; ++i) {
		ktheta = K * theta_k;
		while (ktheta > TWO_PI) ktheta -= TWO_PI;
		R = f_sin (ktheta);
		point<float>& pi = points[i];
		pi.x = center.x + R * f_cos (theta);
		pi.y = center.y + R * f_sin (theta);
		theta += dtheta;
		theta_k += dtheta;
		while (theta > TWO_PI) theta -= TWO_PI;
	}

	ss.str("");
	ss << "rose_" << N << "by" << D << "_" << (int)angle.end;

	gen_pts ();

}

void rose_milker::sin_cos_radius_optioned () {
  do_render ();
}

void rose_milker::sin_cos_radius_edited () {
  do_render ();
}
