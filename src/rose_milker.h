/*
* rose_milker.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit https://dinisnoise.org/
*/

#ifndef __ROSE_MILKER
#define __ROSE_MILKER

#include "spinner.h"
#include "plugin.h"
#include "point.h"
#include "curve_editor.h"
#include "curve_library.h"
#include "funktion.h"
#include "constant_radius.h"
#include "sine.h"
#include "cosine.h"
#include "custom_periodic.h"
#include "ui_sin_cos_radius.h"

#include <vector>

struct rose_milker : plugin, curve_listener, ui_sin_cos_radius_listener {
  //
  // turns rose curve into waveform
  //
  // R = sin (K * theta)
  //
  // K = N / D
  //
  //
  
  // for dealing with custom/standard sin, cos, radius (ignored)
  ui_sin_cos_radius scr;
  void sin_cos_radius_optioned ();
  void sin_cos_radius_edited ();

  point<float> center;
  int N;
  int D;
  float K;
  int num_points; 
  struct {
    float start, end;
  } angle;

  float theta;
  float dtheta;

  struct spinners {
   spinner<int> N;
   spinner<int> D;
   spinner<int> num_points;
   struct {
     spinner<float> start;
     spinner<float> end;
   } angle;
  } sp;

  rose_milker ();
  ~rose_milker ();
  void load_params ();
  void save_params ();

  void setup ();
  void render ();

};

extern rose_milker rosemilker;

#endif



