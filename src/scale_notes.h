/*
* scale_notes.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __scale_notes
#define __scale_notes

#include "label.h"
#include "checkbutton.h"
#include "scale_info.h"

#include <string>
#include <vector>

extern scale_info scaleinfo;
extern int NUM_INTERVALS;
extern std::vector<std::string> INTERVAL_NAMES;

struct scale_notes : widget, state_listener {

  label lbl;

	static const int MAX_INTERVALS = 16; // non-general :( 
	checkbutton cb_notes [MAX_INTERVALS];
  
  void set_pos (int x, int y);
  int handle_input ();
  void draw ();
  void setup ();
  void update ();
  void refresh ();

  template <typename T> void set_color (T r, T g, T b) {
    lbl.set_color (r, g, b);
  }

  void changed (checkbutton& cb);

};

#endif



