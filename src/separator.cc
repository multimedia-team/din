/*
* separator.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/

#include "separator.h"

void separator::draw () {
	widget::draw ();
	glEnable (GL_LINE_STIPPLE);
	glLineStipple (1, 0x1111);
  glBegin (GL_LINES);
    if (type == horizontal) {
      glVertex2i (extents.left, extents.bottom);
      glVertex2i (extents.right, extents.bottom);
    } else {
      glVertex2i (extents.left, extents.bottom);
      glVertex2i (extents.left, extents.top);
    }
  glEnd ();

	/*int pts[4]={0};
	if (type == horizontal) {
    pts[0]=extents.left;pts[1]=extents.bottom;
    pts[2]=extents.right;pts[3]=extents.bottom;
	} else {
    pts[0]=extents.left;pts[1]=extents.bottom;
    pts[2]=extents.left;pts[3]=extents.top;
	}
	glVertexPointer (2, GL_INT, 0, pts);
  glDrawArrays (GL_LINES, 0, 2);*/

	glDisable (GL_LINE_STIPPLE);
}
