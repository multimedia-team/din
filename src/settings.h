/*
* settings.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __settings
#define __settings

#include "ui.h"
#include "button.h"
#include "options_list.h"
#include "checkbutton.h"
#include "label_field.h"
#include "separator.h"
#include "arrow_button.h"
#include "spinner.h"
#include "scale_notes.h"

#include <string>
#include <vector>

struct display_mode {
	int w, h;
	int fullscreen;
	display_mode (int _w = 800, int _h = 600, int _fs = 0) {
		w = _w;
		h = _h;
		fullscreen = _fs;
	}
};

struct delay;
struct instrument;
struct settings : ui, click_listener, option_listener, state_listener, change_listener<field> {

  instrument* curin;

  button b_go_back;
  button b_factory_reset;

	spinner<float> sp_key;
	options_list ol_nearest_note;

  options_list ol_notation;
  std::string current_notation;

  options_list ol_tuning;
  std::vector<std::string> tunings;
  std::string s_current_tuning;
  int num_tunings;

  spinner<int> sp_num_octaves;

  scale_notes sn_scale_notes;

  options_list ol_midi_devices;
  int i_current_midi;
  std::string s_current_midi;
  button b_refresh_midi_devices;

  checkbutton cb_show_midi_messages;

  options_list ol_audio_devices;
  options_list ol_sample_rates;
  options_list ol_buffer_sizes;
  int i_current_device;
  std::string s_current_device;
  button b_refresh_audio_devices;
  
  label_field l_delay_dur;
  label_field r_delay_dur;
	checkbutton cb_sync_delay;
	void do_delay_dur (delay&, field& f);

	separator h_sep1, h_sep2, h_sep3, h_sep4, h_sep5, h_sep6, h_sep7, h_sep8, h_sep9; 

	//arrow_button scroll_up, scroll_down;

	options_list ol_display_modes;
	std::vector<display_mode> display_modes;
	int imode;
	int num_modes;
	void load_fullscreen_modes ();
	void add_display_mode (int w, int h);
	void update_mode_display ();
	void update_windowed_mode (int w, int h);

	spinner<int> sp_fps; // frames per second
	spinner<int> sp_ips; // inputs per second

	int num_widgets;
	std::vector<widget*> widgets;
	std::vector<int> lnspc;
	void move_widgets (int dy);

  std::vector<int> buffer_sizes;
  int i_buffer_size;
  void set_buffer_size (int bs);

	label_field lf_mixing_time; 
	label_field lf_mixing_samples;
	button b_set_to_audio_buffer_time;

  settings ();
  ~settings ();

  void setup ();

  void enter ();

  void update_widgets ();
  void draw ();

  int handle_input ();
  void clicked (button& b);
  void picked (label& l, int dir);
  void changed (checkbutton& cb);
  void changed (field& f);
	void sample_rate_changed ();

};

#endif



