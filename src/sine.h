/*
* sine.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __SINE
#define __SINE

#include <math.h>
#include "funktion.h"

struct sine : funktion {
  float operator() (float a, float ea) {
    return sin (a);
  }
};

#endif



