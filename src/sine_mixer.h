/*
* sine_mixer.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __sine_mixer
#define __sine_mixer

#include "box.h"
#include "multi_curve.h"
#include "spinner.h"
#include "checkbutton.h"
#include "state_button.h"
#include "levels.h"
#include "plugin.h"
#include "point.h"
#include "custom_periodic.h"
#include "sine.h"
#include "curve_editor.h"
#include "listeners.h"
#include "filled_button.h"
#include "mouse_slider.h"
#include "plus_button.h"
#include "minus_button.h"
#include "cross_button.h"

#include <vector>

struct harmonics_list_button : state_button {
	std::vector<int> harmonics;
};

struct sine_mixer : plugin, change_listener<levels>, shift_listener, paint_listener, curve_listener, option_listener {

  static int NUM_SINE_SAMPLES;
  static const int MIN_SINE_SAMPLES;

	int num_points;
	int make_shapeform; 

  funktion* pf_sin;
  custom_periodic cp_sin;
  sine st_sin;
  int type;
  curve_editor sin_ed;

  int nharmonics;
  std::vector< std::vector< point<float> > > harmonics;

  // output
  std::vector< point<float> > norm;
  void normalise ();

  // ui
	//

  spinner<int> sp_points;
  checkbutton cb_make_shapeform;

  // standard/custom sin
  options_list ol_sin;

  button b_edit;

  levels harms; // harmonics
  levels phase; // phase
  levels* levs [2]; // harmonics, phase
  checkbutton cbpaint;
  options_list olpaint;
  MAKE_OPTION_LISTENER(paintlis, pail)
  void toggpaint (label& l);

  sine_mixer ();
  ~sine_mixer ();
	void load_params ();
	void save_params ();

  void setup ();

  void set_type ();
  void set_samples (int s);

  void prep_harmonics ();
  void num_harmonics (int n);
  void mix ();
  void render ();

	int auto_apply;
	void apply_not_auto_apply ();
	void shift_apply ();

  void changed (field& f);
	void changed (checkbutton& cb);

  void changed (levels& l);
  void paint (levels& l);
  void shifted (levels& l);

  void picked (label& lbl, int dir);
  void edited (curve_editor* e, int i);
  void clicked (button& b);
	void moused (int dir, double scl);
  int handle_input ();

  int paintphase;

  int titlemoving;
  MAKE_MOVE_LISTENER (szrmov, szm)

};

extern sine_mixer sinemixer;
#endif
