/*
* slit.h
* slit in Mondrian
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __slit__
#define __slit__

#include <list>
#include "fader.h"

struct rect;

struct slit { // a slit on an edge of a box

	rect* boxes [2]; // slit can be on 2 boxes 
	int edges [2]; // edges of the boxes that has the slit

	int type;
	enum {INVALID=-1, VERTICAL, HORIZONTAL};

	float start, mid, end;
	static float HALF_SIZE;
	static float MIN_HALF_SIZE;
	static float MIN_SIZE; // 2 * MIN_HALF_SIZE

	static int ref; // log

	double animt; // animation duration
	fader* fdr; // to animate slit
	static const double INITIAL_OPEN_CLOSE_TIME;

	slit ();
	slit (rect** bxs, float x, float y, float sz, fader* fdr = 0);
	~slit ();
	int is_too_small ();
	inline void calc_mid () { mid = (start+end) / 2.0; }

	float anim_start, anim_end; // start,end at start of animation
	void toggle_anim ();
	void eval_anim ();

	int select; // selected?

};

struct slit_info {
	float x, y, half_size;
	int anim;
	fader fdr;
	slit_info ();
};

struct slit_lip_t { // for editing slit
	slit* slitt;
	float* lip;
	float* prev;
	float* cur;
	float low, high;
	slit_lip_t () { clear ();}
	void edit ();
	void clear ();
	void set_high (float h);
	void set_low (float l);
};

struct slit_drawer { // for drawing
	static const int MAX_LINES = 512;
	float* verts;
	int cur;
	int last;
	slit_drawer (int n = MAX_LINES);
	~slit_drawer ();
	void add (float x, float y);
	void draw ();
};

void draw_slits (float start, float end, float level, int type, std::list<slit*>& _slits, slit_drawer& slit_drawerr);
rect* get_other_rect_of_slit (rect* ir, int e, float v, slit** os); 
slit* slit_hit (rect* b, int e, float v);
slit* slit_hit (rect** bxs, float x, float y);
int get_slit_lip (slit_lip_t& sl, rect* R, int e, float v);

typedef std::list<slit*>::iterator slit_iterator;

#endif
