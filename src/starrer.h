/*
* starrer.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __STARRER
#define __STARRER

#include "spinner.h"
#include "plugin.h"

struct multi_curve;
struct curve_editor;

struct starrer : plugin {
  //
  // star polygon maker
  //
  int step;
  spinner<int> sp_num_steps;
  starrer ();
  ~starrer ();
  void load_params ();
  void save_params ();
  void setup ();
  void render () {}
  void draw (curve_editor* ed) {}
  int apply (multi_curve& crv);
};

#endif



