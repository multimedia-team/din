/*
* state_button.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/
#ifndef __state_button
#define __state_button
#include "checkbutton.h"
struct state_button : checkbutton {
	static const int SIZE = 8;
	static const int SIZE2 = 2 * SIZE;
	state_button (int sz = SIZE) {set_size (sz);}
	void draw () {draw_bbox ();}
};
#endif
