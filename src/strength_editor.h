/*
* strength_editor.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __STRENGTH_EDITOR
#define __STRENGTH_EDITOR

#include "curve_editor.h"
#include <string>
using namespace std;

struct note_marker {

  string note_name;
  int x;
  static int ybot, ytop;
  static int clearance;

};

struct din;

struct strength_editor : curve_editor {

  din* d;

  vector<note_marker> note_markers;

  int selected_resonator;

  help helptext;

  void build_note_markers ();
  void draw_note_markers ();

  strength_editor (din* dd, string settings_file, string help_file);

  bool handle_input ();

  void select_curve (int i);
  void draw_selected_resonator ();
  void draw ();

};

#endif



