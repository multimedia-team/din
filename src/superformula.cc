/*
* superformula.cc
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* DIN Is Noise is released under GNU Public License 2.0
* For more information, please visit https://dinisnoise.org/
*/


#include "superformula.h"
#include "vector2d.h"
#include "ui_list.h"
#include "console.h"
#include <vector>
using namespace std;

extern ui_list uis;

extern const float PI_BY_180;

superformula::superformula () {
  name = "Superformula";
  center_x = 0.5;
  center_y = 0;
  load_params ();
}

superformula::~superformula () {
  widget_save ("d_superformula", ctrls);
  save_params ();
}

void superformula::load_params () {
  ifstream f (make_fname().c_str(), ios::in);
  string ignore;
	f >> ignore >> m >> ignore >> n1 >> ignore >> n2 >> ignore >> n3 >> ignore >> num_points;

	f >> ignore >> nbookmarks;
	bookmarks.resize(nbookmarks);
	for (int i = 0; i < nbookmarks; ++i) {
		bookmark& bi = bookmarks[i];
		f >> bi.name >> bi.m >> bi.n1 >> bi.n2 >> bi.n3;
		//il_bookmarks.add (bi.name);
	}

}

void superformula::save_params () {
  ofstream f (make_fname().c_str(), ios::out);
  string ignore;
  f << "m " << m << endl << "n1 " << n1 << endl << "n2 " << n2 << endl << "n3 " << n3 << endl << "num_points " << num_points << endl;

	f << "num_bookmarks " << nbookmarks << endl;
	for (int i = 0; i < nbookmarks; ++i) {
		bookmark& bi = bookmarks[i];
		f << bi.name << spc << bi.m << spc << bi.n1 << spc << bi.n2 << spc << bi.n3 << endl;
	}

}

void superformula::setup () {

  plugin::setup ();

  widget* _ctrls [] = {&sp_m, &sp_n1, &sp_n2, &sp_n3, &sp_num_points, &b_browse};
  for (int i = 0; i < 6; ++i) ctrls.push_back (_ctrls[i]); 
  num_ctrls = ctrls.size ();

	b_browse.set_text ("Browse");
	LISTEN(b_browse, &bl);

	//MOVE (b_browse);

  const char* lbl [] = {"m", "n1", "n2", "n3"};
  float val [] = {m, n1, n2, n3};
	spinner<float>* spn [] = {&sp_m, &sp_n1, &sp_n2, &sp_n3};
  for (int i = 0; i < 4; ++i) {
    spinner<float>* spi = spn[i];
    spi->set (lbl[i], 0.01f, 0.01f, MILLION, this, 0);
    spi->set_value (val[i]);
  }

	sp_num_points.set ("Points", 1, 0, MILLION, this, 0);
	sp_num_points.set_value (num_points);

  widget_load ("d_superformula", ctrls);

  render ();

  //for (int i = 0; i < num_ctrls; ++i) ctrls[i]->set_moveable (1);

}

void superformula::render () {

  num_points = sp_num_points.f_value;
  if (num_points == 0) return;

	m = sp_m.f_value; 
  n1 = sp_n1.f_value; 
  n2 = sp_n2.f_value; 
  n3 = sp_n3.f_value;


  theta = 0;
  dtheta = 360.0 / num_points;

	int j = num_points + 1;
	points.resize (j);
  float x, y;
  for (int i = 0; i < j; ++i) {
		float rtheta = theta * PI_BY_180;
		float mtheta = m * rtheta / 4.;
		float t1 = pow (fabs(cos (mtheta)), n2);
		float t2 = pow (fabs(sin (mtheta)), n3);
		float r = pow (t1 + t2, 1.0f / n1);
		if (r == 0) {
			x = center_x;
			y = center_y;
		} else {
			r = 1. / r;
			x = center_x + r * cos (rtheta);
			y = center_y + r * sin (rtheta);
		}
		point<float>& pi = points[i];
		pi.x = x;
		pi.y = y;
    theta += dtheta;
  }

  ss.str("");
  ss << "superformula_" << m << '_' << n1 << '_' << n2 << '_' << n3;

	gen_pts ();

}

void superformula::browse (int i) {
	bid += i;
	wrap (0, bid, nbookmarks-1);
	bookmark& bi = bookmarks[bid];
	cons << "Browsing shape " << bi.name << eol;
	sp_m.set_value (bi.m);
	sp_n1.set_value (bi.n1);
	sp_n2.set_value (bi.n2);
	sp_n3.set_value (bi.n3);
	render ();
	try_auto_apply ();
}

CLICKED_BUTTON(superformula,browse_lis) {
	superformula_.msbl.name = "shape";
	mouse_slider0.add (&superformula_.msbl);
	activate_mouse_slider ();
}

MOUSED(superformula,ms_browse_lis) {
	superformula_.browse (dir);
}

AFTER_SLIDE(superformula,ms_browse_lis) {}
