/*
* superformula.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __SUPERFORMULA
#define __SUPERFORMULA

#include "spinner.h"
#include "plugin.h"
#include "mouse_slider.h"

//#include "item_list.h"


struct superformula : plugin {

	typedef struct {
		std::string name;
		float m, n1, n2, n3;
	} bookmark;

  float center_x, center_y;

	float m, n1, n2, n3;

	int num_points; 
  
  float theta;
  float dtheta;

	spinner<float> sp_m, sp_n1, sp_n2, sp_n3;
  spinner<int> sp_num_points;

	//item_list il_bookmarks;
	std::vector<bookmark> bookmarks;
	int nbookmarks;
	button b_browse;
	int bid;
	MAKE_CLICK_LISTENER (browse_lis, bl)
	MAKE_MOUSE_SLIDER_LISTENER (ms_browse_lis, msbl)
	void browse (int dir);

  superformula ();
  ~superformula ();
  void load_params ();
  void save_params ();

  void setup ();
  void render ();

};

extern superformula superformula_;

#endif
