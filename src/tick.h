/*
* tick.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __tick
#define __tick

#include <time.h>

inline double ticks2secs (clock_t ticks) {
  return ticks * 1.0 / CLOCKS_PER_SEC;
}

inline clock_t secs2ticks (double secs) {
  return (clock_t) (secs * CLOCKS_PER_SEC);
}

#endif



