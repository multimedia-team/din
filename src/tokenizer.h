/*
* tokenizer.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


// string tokenizer
// based on public domain code by Song Ho Ahn (song.ahn@gmail.com)

#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <string>
#include <vector>

extern const char spc;

struct tokenizer {

  tokenizer();
  tokenizer(const std::string& s);
  tokenizer (const std::vector<std::string>& tokens);

  tokenizer& operator>> (std::string& s);
  tokenizer& operator>> (float& f);
  tokenizer& operator>> (double& d);
  tokenizer& operator>> (int& i);
  tokenizer& operator>> (char& c);
  tokenizer& operator>> (unsigned char& c);

  std::vector<std::string> tokens;
  int avail;
  int ntok;
  int id;

  std::string buf;
  int len;
  void str (const std::string& s);
  void vec (const std::vector<std::string>& v);

  static char delim;
  void skipws ();

  int cur;

  std::string cur2end ();
  std::string token;

};
#endif
