/*
* trail.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __TRAIL
#define __TRAIL

#include <list>
#include <string>
#include "point.h"

struct trail_t {

	static float* tpts; 
	static int n_tpts;
  static void alloc (int n);
  static void destroy () { if (tpts) delete[] tpts; }

  struct pts_t {
    std::list <float> x;
    std::list <float> y;
  } pts;
  point<float> last;

  int cur, total;

  trail_t ();
	trail_t (int tp);
	void add (float x, float y);
  void change (int delta);
  void set (int t);
  void reset ();
	void draw ();

#ifdef __SVG__
  void write ();
#endif

};

#endif
