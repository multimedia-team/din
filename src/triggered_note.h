/*
* triggered_note.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/

#ifndef __TRIGGERED_NOTE
#define __TRIGGERED_NOTE

#include "note.h"
#include "solver.h"
#include "play.h"
#include "noiser.h"

struct multi_curve;

struct triggered_note {

  note tn; // note info
  float start_hz; // initial frequency of note

	// play note (default) or noise?
	enum {MIDI = -1, NOTE, NOISE};
	int what;
  solver sol;
  play player;
	noiser nsr;

  // state
  enum {ATTACK, DECAY, FINISHED};
  int state; 

	struct volume_t {
		float now;
		float max;
		float mult;
	} volume;

  //
  // attack
  float abs_attack, delta_attack;
  solver attack; // for attack curve
  float startt;

  //
  // decay
  float decay_time;
  float decay_start_volume;
  float abs_decay, delta_decay;
	float decay_lastx; 
  solver decay; // for decay curve from instrument

  // visual
  int x, y; // position
  float r, g, b; // color

	// for bend
  int prev_mousex; // last mouse pos
  int bend_x;
	int bend;

	// input (used by keyboard-keyboard only)
  int key; // key on computer keyboard that triggers note

  triggered_note (const note& n, float vmax, float xx = 0, float yy = 0, int _what = NOTE, int k = 0, int mox = 0);
  void setup (multi_curve* wav, multi_curve* atk, multi_curve* dk);
  void set_freq (float f);
  void eval (float* left, float* right, float* wav, float* vol, int n, float attack_time, float decay_time, gotog& g);
  void eval (float attack_time);
  void start_decay ();
	int update_solver (multi_curve& mix, const std::string& nam);

};

typedef std::list<triggered_note>::iterator note_iterator;
void update_triggered_notes (std::list<triggered_note>& tl);
void update_triggered_noises (std::list<triggered_note>& tl);

#endif
