/*
* warper.h
* DIN Is Noise is copyright (c) 2006-2024 Jagannathan Sampath
* For more information, please visit http://dinisnoise.org/
*/


#ifndef __WARPER
#define __WARPER

#include "spinner.h"
#include "curve_editor.h"
#include "curve_display.h"
#include "curve_library.h"
#include "plugin.h"

struct warper;
struct warp_listener : curve_listener {
	warper* wp;
  void edited (curve_editor* ed, int i);
};

struct warper : plugin {

	multi_curve x, y;
	solver solx, soly;

	warp_listener lis;
  curve_editor ed;
	curve_editor* back_ed;

  button b_edit;

  void clicked (button& b);

  warper ();
  ~warper ();

  void setup ();
	void warp (float& x, float& y, const box<float>& bbox);
  void render ();
  int apply (multi_curve& crv);
  void draw (curve_editor* ed);

};

extern warper warper_;
#endif



